plugins {
    id("com.singerinstruments.tcats.kotlin-library-conventions")
}

dependencies {
    implementation(project(":json"))
    implementation(project(":data"))
    implementation(project(":dataservices"))
    implementation(project(":model"))
}