package com.singerinstruments.tcats.viewmodel

import com.singerinstruments.tcats.data.UnitTypeData
import com.singerinstruments.tcats.model.TechUnitType
import com.singerinstruments.tcats.model.UnitTypeModel

public class UnitTypesViewModel(model: UnitTypeModel) {
    public interface Listener {
        public fun changed(viewModel: UnitTypesViewModel)
    }

    public var model: UnitTypeModel = model
        set(value) {
            field = value
            notifyListeners()
        }

    private val listeners = mutableListOf<Listener>()
    public fun add(listener: Listener): Boolean = this.listeners.add(listener)
    public fun remove(listener: Listener): Boolean = this.listeners.remove(listener)
    private fun notifyListeners() {
        this.listeners.forEach { it.changed(this) }
    }

    public val techUnitTypes: List<TechUnitType> get() = this.model.toList()

    public fun refresh() {
        this.model.refresh()
        this.notifyListeners()
    }

    public fun createModuleType(typeData: UnitTypeData) {
        this.model.createModuleType(typeData)
        this.model.refresh()
        this.notifyListeners()
    }
}