package com.singerinstruments.tcats.viewmodel

import com.singerinstruments.tcats.model.Model

/**
 * Links view modelViews and their updates.  Not sure if updating should happen in
 * model layer instead......
 */
public class ViewModel(model: Model = Model()) {

    public companion object {
        public val fileExtension: String = "tcats"
        public val fileExtensionWithDot: String = "." + this.fileExtension
    }

    public var model: Model = model
        set(value) {
            field = value
            this.actionsViewModel.model = model.actions
            this.unitTypesViewModel.model = model.unitTypes
            this.workflowsViewModel.model = model.workflows
            this.wallsViewModel.model = model.walls

            this.actionsViewModel.refresh()
            this.unitTypesViewModel.refresh()
            this.wallsViewModel.refresh()
            this.workflowsViewModel.refresh()
        }

    public val actionsViewModel: ActionsViewModel = ActionsViewModel(this.model.actions)
    public val unitTypesViewModel: UnitTypesViewModel = UnitTypesViewModel(this.model.unitTypes)
    public val workflowsViewModel: WorkflowsViewModel = WorkflowsViewModel(this.model.workflows)
    public val wallsViewModel: WallsViewModel = WallsViewModel(this.model.walls)

    private val actionsListener = object : ActionsViewModel.Listener {
        override fun changed(viewModel: ActionsViewModel) {
            this@ViewModel.unitTypesViewModel.refresh()
            this@ViewModel.workflowsViewModel.refresh()
        }
    }

    private val typesListener = object : UnitTypesViewModel.Listener {
        override fun changed(viewModel: UnitTypesViewModel) {
            this@ViewModel.wallsViewModel.refresh()
        }
    }

    init {
        this.actionsViewModel.add(actionsListener)
        this.unitTypesViewModel.add(typesListener)
    }
}

