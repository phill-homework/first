package com.singerinstruments.tcats.viewmodel

import com.singerinstruments.tcats.model.Action
import com.singerinstruments.tcats.model.ActionModel

public class ActionsViewModel(model: ActionModel) {

    internal var model = model
        set(value) {
            field = value
            notifyListeners()
        }

    public interface Listener {
        public fun changed(viewModel: ActionsViewModel)
    }

    private val listeners = mutableListOf<Listener>()
    public fun add(listener: Listener): Boolean = this.listeners.add(listener)
    public fun remove(listener: Listener): Boolean = this.listeners.remove(listener)
    private fun notifyListeners() {
        this.listeners.forEach { it.changed(this) }
    }

    public val actions: List<Action> get() = this.model.toList()


    public fun refresh() {
        this.model.refreshActionData()
        notifyListeners()
    }

    public fun addScienceAction(name: String) {
        this.model.addScienceAction(name)
        this.refresh()
        notifyListeners()
    }
}

