/*
 * Copyright (c) 2019. Phill van Leersum
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.singerinstruments.tcats.viewmodel

import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate
import uk.vanleersum.kotlin.json.Unparser
import java.io.*
import java.util.*

public data class Rectangle(public val x: Int, public val y: Int, public val width: Int, public val height: Int)

public class Preferences {

    private val file: File
    private val map = HashMap<String, String>()

    private val jsonDelegate = object : ParserDelegate {
        override fun jsonDidParseString(parser: Parser, key: String?, value: String) {
            if (null != key) {
                this@Preferences.map[key] = value
            }
        }
    }

    private constructor() {
        val userDir = System.getProperty("user.home")
        val file = File(userDir)
        this.file = File(file, ".tcats.preferences")
        this.read()
    }

    private constructor(file: File) : super() {
        this.file = file
        this.read()
    }

    public fun bounds(): Rectangle? {
        val string = this.map["bounds"]
        if (null != string && !string.isEmpty()) {
            val i = string.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (4 == i.size) {
                try {
                    val x = Integer.parseInt(i[0])
                    val y = Integer.parseInt(i[1])
                    val width = Integer.parseInt(i[2])
                    val height = Integer.parseInt(i[3])
                    return Rectangle(x, y, width, height)
                } catch (e: Exception) {
                }

            }
        }
        return null
    }

    public fun bounds(rectangle: Rectangle) {
        val str = String.format("%d,%d,%d,%d", rectangle.x, rectangle.y, rectangle.width, rectangle.height)
        this.map["bounds"] = str
        this.write()
    }

    public fun currentFileName(): String? {
        return this.map["currentFileName"]
    }

    public fun currentFileName(filename: String) {
        this.map["currentFileName"] = filename
        this.write()
    }

    private fun read() {
        if (!this.file.exists()) {
            return
        }
        try {
            BufferedReader(FileReader(this.file)).use { reader ->
                val parser = Parser(reader, this.jsonDelegate)
                parser.parse()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun write() {
        try {
            BufferedWriter(FileWriter(this.file)).use { writer ->
                val unparser = Unparser(writer)
                unparser.pretty = true
                unparser.beginMap()
                for ((key, value) in this.map) {
                    unparser.keyValue(key, value)
                }
                unparser.endMap()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    public companion object {
        public val Shared: Preferences = Preferences()
    }


    private fun currentDirectory(): File {
        var currentDirectoryName = System.getProperty("user.home")
        val currentFileName = this.currentFileName()

        if (null != currentFileName && currentFileName.isNotEmpty()) {
            var f = File(currentFileName)
            f = f.parentFile
            currentDirectoryName = f.absolutePath
            try {
                currentDirectoryName = f.canonicalPath
            } catch (e: Exception) {
            }
        }
        return File(currentDirectoryName)
    }
}
