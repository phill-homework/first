package com.singerinstruments.tcats.viewmodel

import com.singerinstruments.tcats.data.WallData
import com.singerinstruments.tcats.model.UnitTypeSource
import com.singerinstruments.tcats.model.Wall
import com.singerinstruments.tcats.model.WallModel

public class WallsViewModel(model: WallModel) {
    public interface Listener {
        public fun changed(viewModel: WallsViewModel)
    }

    internal var model = model
        set(value) {
            field = value
            notifyListeners()
        }

    public val unitTypeSource: UnitTypeSource get() = model.unitTypeSource
    private val listeners = mutableListOf<Listener>()
    public fun add(listener: Listener): Boolean = this.listeners.add(listener)
    public fun remove(listener: Listener): Boolean = this.listeners.remove(listener)
    private fun notifyListeners() {
        this.listeners.forEach { it.changed(this) }
    }

    public val installations: List<Wall> get() = this.model.toList()


    public fun refresh() {
        this.model.refresh()
        this.notifyListeners()
    }

    /**
     * create and return an installation referencing the underlying model, but not actually part of it
     */
    public fun createInstallation(): Wall = this.model.createInstallation()

    public fun putInstallationData(installationData: WallData): Unit
        = this.model.putInstallationData(installationData)

}