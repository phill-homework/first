package com.singerinstruments.tcats.viewmodel

import com.singerinstruments.tcats.model.Workflow
import com.singerinstruments.tcats.model.WorkflowModel

public class WorkflowsViewModel(model: WorkflowModel) {
    public interface Listener {
        public fun changed(viewModel: WorkflowsViewModel)
    }

    internal var model = model
        set(value) {
            field = value
            notifyListeners()
        }

    private val listeners = mutableListOf<Listener>()
    public fun add(listener: Listener): Boolean = this.listeners.add(listener)
    public fun remove(listener: Listener): Boolean = this.listeners.remove(listener)
    private fun notifyListeners() {
        this.listeners.forEach { it.changed(this) }
    }

    public val workflows: List<Workflow> get() = this.model.toList()


    public fun refresh() {
        this.model.refreshWorkflows()
        this.notifyListeners()
    }

    public fun addWorkflow(name: String) {
        this.model.addWorkflow(name)
        this.notifyListeners()
    }
}

