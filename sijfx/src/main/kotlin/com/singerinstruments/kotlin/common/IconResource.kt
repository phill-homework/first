package com.singerinstruments.kotlin.common


import com.singerinstruments.tcats.data.StandardActions
import javafx.scene.image.Image

public object IconResource {

    private fun getImage(string: String) = try {
            Image(javaClass.getResource(string).toString(),
                0.0, 0.0,
                true, true, false)
        } catch (e: Exception) {
            null
        }

    public val thumbsUp: Image? by lazy { this.getImage("/thumbsUp.png")}
    public val thumbsDown: Image? by lazy { this.getImage("/thumbsDown.png") }
    public val colonyPicker: Image? by lazy { this.getImage("/moduleicons/colony_picker_trans.png") }
    public val imager: Image? by lazy { this.getImage("/moduleicons/imager_trans.png") }
    public val reader: Image? by lazy { this.getImage("/moduleicons/plate_reader_v2_trans.png") }
    public val stacker: Image? by lazy { this.getImage("/moduleicons/stacker_trans.png") }
    public val incubator: Image? by lazy { this.getImage("/moduleicons/incubator_trans.png") }
    public val refrigerator: Image? by lazy { this.getImage("/moduleicons/refrigerator_trans.png") }
    public val sealer: Image? by lazy { this.getImage("/moduleicons/plate_sealer_trans.png") }
    public val discard: Image? by lazy { this.getImage("/moduleicons/trash.png") }
    public val singer: Image? by lazy { this.getImage("/singer.png") }

    public val map: Map<String, Image?>  =
        mapOf(
            StandardActions.image.uid to this.imager,
            StandardActions.pick.uid to this.colonyPicker,
            StandardActions.read.uid to this.reader,
            StandardActions.seal.uid to this.sealer,
            StandardActions.incubate37.uid to this.incubator,
            StandardActions.incubate30.uid to this.incubator,
            StandardActions.store.uid to this.stacker,
            StandardActions.store4.uid to this.refrigerator,
            StandardActions.storeMinus20.uid to this.refrigerator,
            StandardActions.storeMinus80.uid to this.refrigerator,
            StandardActions.discard.uid to this.discard)

    public operator fun get(uid: String): Image? = this.map[uid]

}