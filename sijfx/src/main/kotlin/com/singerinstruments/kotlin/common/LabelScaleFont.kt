package com.singerinstruments.kotlin.common

import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.RadioButton
import javafx.scene.text.Font
import javafx.scene.text.Text


public fun Label.scaleFont(by: Double) {
    this.font = Font.font(this.font.size * by)
}

public fun Text.scaleFont(by: Double) {
    this.font = Font.font(this.font.size * by)
}

public fun Button.scaleFont(by: Double) {
    this.font = Font.font(this.font.size * by)
}

public fun RadioButton.scaleFont(by: Double) {
    this.font = Font.font(this.font.size * by)
}