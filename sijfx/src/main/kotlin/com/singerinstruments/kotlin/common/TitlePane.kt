package com.singerinstruments.kotlin.common

import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.StackPane
import javafx.scene.paint.Color
import javafx.scene.text.Font
import javafx.scene.text.FontPosture
import javafx.scene.text.FontWeight

public class TitlePane(title: String, node: Node) : AnchorPane() {
    init {
        val label = Label(title)
        //label.opacity = 1.0
        //label.background = Background(BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY))
        val font = Font.font(Font.getDefault().family, FontWeight.NORMAL, FontPosture.ITALIC, Font.getDefault().size * 0.8)
        label.font = font
        label.textFill = Color.GRAY
        label.alignment = Pos.CENTER

        setTopAnchor(label, 0.0)
        setLeftAnchor(label, 0.0)
        setRightAnchor(label, 0.0)

        val rect = StackPane(node)
        rect.style = "-fx-border-color: lightgray"
        setTopAnchor(rect, label.boundsInLocal.maxY)
        setBottomAnchor(rect, 4.0)
        setLeftAnchor(rect, 4.0)
        setRightAnchor(rect, 4.0)

        this.children.addAll(rect, label)

        label.boundsInLocalProperty().addListener { _, _, bounds ->
            setTopAnchor(rect, bounds.maxY)
        }
    }
}