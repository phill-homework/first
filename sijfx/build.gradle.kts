
plugins {
    id("com.singerinstruments.tcats.kotlin-library-conventions")
    id("org.openjfx.javafxplugin") version "0.0.8" // PTvL
}

javafx {
    version = "11.0.2"
    modules = mutableListOf("javafx.controls") //PTvL
}

dependencies {
    implementation( project(":data"))
}
