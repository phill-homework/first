
plugins {
    id("com.singerinstruments.tcats.kotlin-library-conventions")
}

dependencies {
    implementation(project(":dataservices"))
    implementation(project(":data"))
}