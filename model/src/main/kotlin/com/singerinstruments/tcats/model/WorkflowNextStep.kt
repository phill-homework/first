package com.singerinstruments.tcats.model

import com.singerinstruments.tcats.data.WorkflowNextStepData

public class WorkflowNextStep(private val step: WorkflowStep, private val data: WorkflowNextStepData) {
    public val condition: String get() = this.data.condition
    public val nextStepId: Long get() = this.data.nextId
    public val nextStep: WorkflowStep? get() = this.step.version.stepWithId(this.data.nextId)
}