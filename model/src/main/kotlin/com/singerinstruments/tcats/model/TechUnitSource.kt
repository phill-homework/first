package com.singerinstruments.tcats.model

/**
 * A provider of ScienceModules
 */
public interface TechUnitSource : Iterable<TechUnit> {
    public operator fun get(uid: String): TechUnit?
}