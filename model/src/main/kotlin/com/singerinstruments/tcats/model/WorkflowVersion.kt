package com.singerinstruments.tcats.model

import com.singerinstruments.tcats.data.WorkflowVersionData

public class WorkflowVersion(internal val workflow: Workflow, private val data: WorkflowVersionData): Iterable<WorkflowStep> {
    public val steps: List<WorkflowStep> = data.steps.map { WorkflowStep(this, it) }.toList()

    override fun iterator(): Iterator<WorkflowStep>  = this.steps.iterator()

    public val version: Long get() = data.version

    internal fun stepWithId(id: Long): WorkflowStep? = firstOrNull { it.id == id }

}