package com.singerinstruments.tcats.model

/**
 * A provider of StandardActions
 */
public interface UnitTypeSource : Iterable<TechUnitType> {
    public operator fun get(uid: String): TechUnitType?
}