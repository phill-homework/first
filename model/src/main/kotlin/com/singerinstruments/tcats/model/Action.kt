package com.singerinstruments.tcats.model

import com.singerinstruments.tcats.data.ActionData

public data class Action(private val model: ActionModel, private val data: ActionData) {

    public val uid: String = this.data.uid
    public val name: String = this.data.name

    override fun toString(): String = "Action: $name ($uid)"

}

