package com.singerinstruments.tcats.model

import com.singerinstruments.tcats.data.UnitTypeData


public data class TechUnitType(private val model: UnitTypeModel, private val data: UnitTypeData) {

    public val uid: String = this.data.uid
    public val name: String = this.data.name
    public val actionIds: List<String> get() = this.data.actionIds
    public val actions: List<Action> get() = this.actionIds.mapNotNull { model.scienceAction(it) }
    override fun toString(): String = "TechUnit: ${this.name}"

}

