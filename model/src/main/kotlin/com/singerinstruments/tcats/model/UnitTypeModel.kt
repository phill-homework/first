package com.singerinstruments.tcats.model

import com.singerinstruments.tcats.data.UnitTypeData
import com.singerinstruments.tcats.data.UnitTypeDataList
import com.singerinstruments.tcats.dataservices.UnitTypeDataService

public class UnitTypeModel(
    private val dataService: UnitTypeDataService,
    public val actionSource: ActionSource
) : Iterable<TechUnitType>, UnitTypeSource {

    private var moduleTypeMap = mutableMapOf<String, TechUnitType>()

    override fun iterator(): Iterator<TechUnitType> = this.moduleTypeMap.values.toList().iterator()

    override operator fun get(uid: String): TechUnitType? = this.moduleTypeMap[uid]
    public val data: UnitTypeDataList = dataService.getModuleTypeData()

    public fun refresh() {
        this.moduleTypeMap = dataService.getModuleTypeData()
            .map { it.uid to TechUnitType(this, it) }
            .toMap().toMutableMap()
    }

    internal fun scienceAction(uid: String) = this.actionSource[uid]

    public fun createModuleType(typeData: UnitTypeData) {
        dataService.createModuleType(typeData)
    }
}