package com.singerinstruments.tcats.model

import com.singerinstruments.tcats.data.DataModel
import com.singerinstruments.tcats.dataservices.DataService
import com.singerinstruments.tcats.dataservices.ephemeral.EphemeralDataService

/**
 * A container for the individual models.
 *
 * Main task seems to be setting up dependencies between models
 */
public class Model(dataService: DataService = EphemeralDataService()) {

    public val actions: ActionModel = ActionModel(dataService.actionDataService)

    public val data: DataModel
        get() = DataModel(
            this.walls.data,
            this.workflows.data,
            this.unitTypes.data,
            this.actions.data
        )

    public val workflows: WorkflowModel = WorkflowModel(
        dataService.workflowDataService,
        this.actions
    )

    public val unitTypes: UnitTypeModel = UnitTypeModel(
        dataService.unitTypeDataService,
        this.actions
    )

    public val walls: WallModel = WallModel(dataService.wallDataService, this.unitTypes)
}