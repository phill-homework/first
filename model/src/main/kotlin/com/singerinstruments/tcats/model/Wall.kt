package com.singerinstruments.tcats.model

import com.singerinstruments.tcats.data.WallData

public class Wall(
    internal val unitTypeSource: UnitTypeSource,
    public val data: WallData
) : TechUnitSource {

    private var moduleMap = this.data
        .map { it.uid to TechUnit(this, it) }
        .toMap()

    override fun iterator(): Iterator<TechUnit> = this.moduleMap.values.iterator()
    override operator fun get(uid: String): TechUnit? = this.moduleMap[uid]

    public val units: List<TechUnit> = this.moduleMap.values.toList()
    public val uid: String = this.data.uid
    public val name: String = this.data.name

    internal fun unitType(uid: String) = this.unitTypeSource[uid]

    public val availableActions: List<Action>
        get() = this.units
            .flatMap { it.actions }
            .distinct()
            .sortedBy { it.name }
}