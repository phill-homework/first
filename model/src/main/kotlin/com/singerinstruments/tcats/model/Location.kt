package com.singerinstruments.tcats.model

import com.singerinstruments.tcats.data.LocationData

public class Location(private val data: LocationData) {
    public val row: Int = this.data.row
    public val column: Int = this.data.column
}