package com.singerinstruments.tcats.model

import com.singerinstruments.tcats.data.WorkflowData

public class Workflow(internal val model: WorkflowModel, private val data: WorkflowData): Iterable<WorkflowVersion> {
    private val versionMap = data.map {
        it.version to WorkflowVersion(this, it)
    }.toMap()

    public val versions: List<WorkflowVersion> = this.versionMap.values.toList()
    public val uid: String = this.data.uid

    override fun iterator(): Iterator<WorkflowVersion>  = this.versions.iterator()

    public val name: String get() = data.name
}

