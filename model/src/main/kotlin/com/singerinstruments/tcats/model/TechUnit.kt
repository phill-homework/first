package com.singerinstruments.tcats.model

import com.singerinstruments.tcats.data.UnitData

public class TechUnit(private val installation: Wall, private val data: UnitData) {
    public val uid: String = this.data.uid
    public val name: String = this.data.name
    public val typeId: String = this.data.moduleTypeId

    public val techUnitType: TechUnitType? get() = this.installation.unitType(this.typeId)
    public val location: Location = Location(this.data.locationData)

    public val actionIds: List<String> get() = this.techUnitType?.actionIds ?: emptyList()
    public val actions: List<Action> get() = this.techUnitType?.actions ?: emptyList()

}