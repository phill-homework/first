package com.singerinstruments.tcats.model

import com.singerinstruments.tcats.data.ActionDataList
import com.singerinstruments.tcats.dataservices.ActionDataService

public class ActionModel(
    private val dataService: ActionDataService,
): Iterable<Action>, ActionSource {

    private var actionMap = mapOf<String, Action>()

    override fun iterator(): Iterator<Action> = this.actionMap.values.toList().iterator()

    override operator fun get(uid: String): Action? = this.actionMap[uid]

    public val data: ActionDataList = dataService.getActionData()

    public fun refreshActionData() {
        val data = this.dataService.getActionData()
        this.actionMap = data
            .map { it.uid to Action(this, it) }
            .toMap()
    }

    public fun addScienceAction(name: String) {
        this.dataService.addAction(name)
        refreshActionData()
    }
}