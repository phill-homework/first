package com.singerinstruments.tcats.model

import com.singerinstruments.tcats.data.WorkflowStepData

public class WorkflowStep(internal val version: WorkflowVersion, private val data: WorkflowStepData) {
    public val nextSteps: List<WorkflowNextStep>
        = data.next
            .map { WorkflowNextStep(this, it) }
            .toList()

    public val actionId: String get() = this.data.actionId
    public val action: Action?
        get() = this.version.workflow.model.scienceAction(data.actionId)

    public val id: Long get() = this.data.id
}

