package com.singerinstruments.tcats.model

import com.singerinstruments.tcats.data.WorkflowDataList
import com.singerinstruments.tcats.dataservices.WorkflowDataService

public class WorkflowModel(
    private val dataService: WorkflowDataService,
    private val actionSource: ActionSource
) : Iterable<Workflow> {

    private var workflows = mutableMapOf<String, Workflow>()
    public val data: WorkflowDataList = dataService.getWorkflowData()
    override fun iterator(): Iterator<Workflow> = this.workflows.values.iterator()

    internal fun scienceAction(uid: String) = this.actionSource[uid]

    public fun refreshWorkflows() {
        this.workflows = dataService.getWorkflowData()
            .map { it.uid to Workflow(this, it) }
            .toMap().toMutableMap()
    }

    public fun addWorkflow(name: String) {
        println("WorkflowModel.addWorkflow($name): Not yet implemented")
    }
}