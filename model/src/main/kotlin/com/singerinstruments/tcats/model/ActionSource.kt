package com.singerinstruments.tcats.model

/**
 * A provider of StandardActions
 */
public interface ActionSource: Iterable<Action> {
    public operator fun get(uid: String): Action?
}


