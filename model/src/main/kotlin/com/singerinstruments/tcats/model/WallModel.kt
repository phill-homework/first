package com.singerinstruments.tcats.model

import com.singerinstruments.tcats.data.LocationData
import com.singerinstruments.tcats.data.UnitData
import com.singerinstruments.tcats.data.WallData
import com.singerinstruments.tcats.data.WallDataList
import com.singerinstruments.tcats.dataservices.WallDataService

public class WallModel(
    private val dataService: WallDataService,
    public val unitTypeSource: UnitTypeSource
) : Iterable<Wall> {

    private var installationMap = mutableMapOf<String, Wall>()

    public val data: WallDataList = dataService.getDataList()

    override fun iterator(): Iterator<Wall> = this.installationMap.values.iterator()

    public operator fun get(uid: String): Wall? = this.installationMap[uid]

    public fun refresh() {
        val installationData = this.dataService.getDataList()
        this.installationMap = installationData
            .map { it.uid to Wall(this.unitTypeSource, it) }
            .toMap()
            .toMutableMap()
    }

    public fun createInstallation(): Wall {
        val type = this.unitTypeSource.first()
        val module = UnitData("unnamed unit", type.uid, LocationData())
        val inst = WallData("unnamed wall", listOf(module))
        return Wall(this.unitTypeSource, inst)
    }

    public fun putInstallationData(installationData: WallData): Unit = this.dataService.putWallData(installationData)
}


