package com.singerinstruments.tcats.dataservices

import com.singerinstruments.tcats.data.WallData
import com.singerinstruments.tcats.data.WallDataList

/**
 * Service supporting WallData
 */
public interface WallDataService {
    public fun getDataList(): WallDataList
    public fun putWallData(data: WallData)
}