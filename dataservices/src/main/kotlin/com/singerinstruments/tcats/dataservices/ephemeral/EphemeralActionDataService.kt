package com.singerinstruments.tcats.dataservices.ephemeral

import com.singerinstruments.tcats.data.ActionDataList
import com.singerinstruments.tcats.dataservices.ActionDataService

/**
 * In-memory local data service.  Primarily used for testing
 */
public class EphemeralActionDataService(public val actionDataList: ActionDataList) :
    ActionDataService {

    override fun getActionData(): ActionDataList = this.actionDataList

    override fun addAction(name: String) {
        actionDataList.add(name)
    }

}

