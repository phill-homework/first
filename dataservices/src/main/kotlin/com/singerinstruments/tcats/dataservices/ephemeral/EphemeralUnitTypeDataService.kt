package com.singerinstruments.tcats.dataservices.ephemeral

import com.singerinstruments.tcats.data.UnitTypeData
import com.singerinstruments.tcats.data.UnitTypeDataList
import com.singerinstruments.tcats.dataservices.UnitTypeDataService

public class EphemeralUnitTypeDataService(public val unitTypeDataList: UnitTypeDataList) :
    UnitTypeDataService {

    override fun getModuleTypeData(): UnitTypeDataList = this.unitTypeDataList

    override fun createModuleType(unitTypeData: UnitTypeData) {
        this.unitTypeDataList.moduleTypeData.add(unitTypeData)
    }

}