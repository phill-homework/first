package com.singerinstruments.tcats.dataservices.ephemeral

import com.singerinstruments.tcats.data.WorkflowData
import com.singerinstruments.tcats.data.WorkflowDataList
import com.singerinstruments.tcats.dataservices.WorkflowDataService

/**
 * In-memory local data service.  Primarily used for testing
 */
public class EphemeralWorkflowDataService(public val workflowDataList: WorkflowDataList) : WorkflowDataService {
    override fun getWorkflowData(): WorkflowDataList = this.workflowDataList

    override fun addWorkflow(name: String) {
        this.workflowDataList.add(WorkflowData(name))
    }
}

