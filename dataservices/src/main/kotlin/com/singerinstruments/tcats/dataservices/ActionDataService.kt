package com.singerinstruments.tcats.dataservices

import com.singerinstruments.tcats.data.ActionDataList

/**
 * A mechanism for retrieving ActionData
 */
public interface ActionDataService {
    public fun getActionData(): ActionDataList
    public fun addAction(name: String)
}

