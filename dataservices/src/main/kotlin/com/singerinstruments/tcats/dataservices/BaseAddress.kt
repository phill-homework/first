package com.singerinstruments.tcats.dataservices

public data class BaseAddress(public val serverPath: String = "http://localhost",
                              public val port: Int = 8081)