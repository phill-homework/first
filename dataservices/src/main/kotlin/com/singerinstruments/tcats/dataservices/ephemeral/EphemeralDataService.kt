package com.singerinstruments.tcats.dataservices.ephemeral

import com.singerinstruments.tcats.data.*
import com.singerinstruments.tcats.dataservices.*

/**
 * In-memory local data service. Primarily used for testing.
 */
public class EphemeralDataService(
    actionDataList: ActionDataList = StandardActions.list,
    unitTypeDataList: UnitTypeDataList = SingerUnitTypeData.list,
    wallDataList: WallDataList = WallDataList(),
    workflowDataList: WorkflowDataList = WorkflowDataList()
) : DataService {

    public constructor(model: DataModel) : this(model.actions, model.unitTypes, model.walls, model.workflows)

    override val actionDataService: ActionDataService = EphemeralActionDataService(actionDataList)

    override val unitTypeDataService: UnitTypeDataService = EphemeralUnitTypeDataService(unitTypeDataList)

    override val wallDataService: WallDataService = EphemeralWallDataService(wallDataList)

    override val workflowDataService: WorkflowDataService = EphemeralWorkflowDataService(workflowDataList)

    override fun stop() { /* no-op */
    }
}