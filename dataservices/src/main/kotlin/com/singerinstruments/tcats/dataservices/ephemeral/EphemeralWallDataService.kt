package com.singerinstruments.tcats.dataservices.ephemeral

import com.singerinstruments.tcats.data.WallData
import com.singerinstruments.tcats.data.WallDataList
import com.singerinstruments.tcats.dataservices.WallDataService

/**
 * In-memory local data service.  Primarily used for testing
 */
public class EphemeralWallDataService(public val wallDataList: WallDataList) : WallDataService {
    override fun getDataList(): WallDataList = this.wallDataList

    override fun putWallData(data: WallData) {
        this.wallDataList.add(data)
    }

}