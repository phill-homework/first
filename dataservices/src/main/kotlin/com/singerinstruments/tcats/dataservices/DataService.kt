package com.singerinstruments.tcats.dataservices

/**
 * A convenient aggregate of the required data services
 */
public interface DataService {
    public val actionDataService: ActionDataService
    public val workflowDataService: WorkflowDataService
    public val unitTypeDataService: UnitTypeDataService
    public val wallDataService: WallDataService
    public fun stop()
}

