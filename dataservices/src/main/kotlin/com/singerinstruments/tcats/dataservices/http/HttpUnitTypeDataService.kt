package com.singerinstruments.tcats.dataservices.http

import com.singerinstruments.tcats.data.UnitTypeData
import com.singerinstruments.tcats.data.UnitTypeDataList
import com.singerinstruments.tcats.dataservices.BaseAddress
import com.singerinstruments.tcats.dataservices.UnitTypeDataService
import org.eclipse.jetty.client.HttpClient
import org.eclipse.jetty.client.util.StringRequestContent

public class HttpUnitTypeDataService(private val httpClient: HttpClient,
                                     private val baseAddress: BaseAddress
                                            ): UnitTypeDataService {

    override fun getModuleTypeData(): UnitTypeDataList {
        this.httpClient.start() // no-op if started
        httpClient.bindAddress
        val path = this.baseAddress.serverPath + ":" + this.baseAddress.port + UnitTypeDataList.logicalPath
        val response = httpClient.GET(path)
        println("HttpUnitTypeDataService " + response.contentAsString)
        return UnitTypeDataList.fromJson(response.contentAsString)
    }

    override fun createModuleType(unitTypeData: UnitTypeData) {
        this.httpClient.start() // no-op if started
        val path = this.baseAddress.serverPath + ":" + this.baseAddress.port + UnitTypeDataList.logicalPath + "/"
        httpClient.POST(path)
            .body(StringRequestContent(unitTypeData.json))
            .send()
    }
}