package com.singerinstruments.tcats.dataservices.http

import com.singerinstruments.tcats.data.WallData
import com.singerinstruments.tcats.data.WallDataList
import com.singerinstruments.tcats.dataservices.BaseAddress
import com.singerinstruments.tcats.dataservices.WallDataService
import org.eclipse.jetty.client.HttpClient
import org.eclipse.jetty.client.util.StringRequestContent

/**
 * In-memory local data service.  Primarily used for testing
 */
public class HttpWallDataService(private val httpClient: HttpClient,
                                 private val baseAddress: BaseAddress )
                                                    : WallDataService {
    override fun getDataList(): WallDataList {
        this.httpClient.start() // no-op if started
        val path = this.baseAddress.serverPath + ":" + this.baseAddress.port + WallDataList.logicalPath
        val response = httpClient.GET(path)
        println("HttpWorkflowDataService " + response.contentAsString)
        return WallDataList.fromJson(response.contentAsString)
    }

    override fun putWallData(data: WallData) {
        this.httpClient.start() // no-op if started
        val path = this.baseAddress.serverPath + ":" + this.baseAddress.port + WallDataList.logicalPath + "/"
        httpClient.POST(path)
            .body(StringRequestContent(data.json))
            .send()
    }

}