package com.singerinstruments.tcats.dataservices.http

import com.singerinstruments.tcats.dataservices.*
import org.eclipse.jetty.client.HttpClient

/**
 * Implementation of a [DataService] sourced from a http server by a httpclient.
 *
 * In this case we are using the built-in Jetty client
 */
public class HttpDataService(baseAddress: BaseAddress): DataService {

    private val httpClient = HttpClient()

    override val actionDataService: ActionDataService
                = HttpActionDataService(httpClient, baseAddress)

    override val workflowDataService: WorkflowDataService
                = HttpWorkflowDataService(httpClient, baseAddress)

    override val unitTypeDataService: UnitTypeDataService
                = HttpUnitTypeDataService(httpClient, baseAddress)

    override val wallDataService: WallDataService
                = HttpWallDataService(httpClient, baseAddress)

    override fun stop(): Unit = this.httpClient.stop()
}