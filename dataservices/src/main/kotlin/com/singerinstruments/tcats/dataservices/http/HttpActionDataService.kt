package com.singerinstruments.tcats.dataservices.http

import com.singerinstruments.tcats.data.ActionDataList
import com.singerinstruments.tcats.dataservices.ActionDataService
import com.singerinstruments.tcats.dataservices.BaseAddress
import org.eclipse.jetty.client.HttpClient

/**
 * A mechanism for retrieving ActionData from a HttpServer
 */
public class HttpActionDataService(private val httpClient: HttpClient,
                                   private val baseAddress: BaseAddress
                                            ): ActionDataService {

    override fun getActionData(): ActionDataList {
        this.httpClient.start() // no-op if started
        httpClient.bindAddress
        val path = this.baseAddress.serverPath + ":" + this.baseAddress.port + ActionDataList.logicalPath
        val response = httpClient.GET(path)
        println("HttpActionDataService " + response.contentAsString)
        return ActionDataList.fromJson(response.contentAsString)
    }

    override fun addAction(name: String) {
        this.httpClient.start() // no-op if started
        val path = this.baseAddress.serverPath + ":" + this.baseAddress.port + ActionDataList.logicalPath + "/"
        httpClient.POST(path)
            .param("name", name)
            .send()
    }
}

