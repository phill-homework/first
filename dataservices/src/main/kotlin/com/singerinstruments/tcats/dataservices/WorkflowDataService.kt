package com.singerinstruments.tcats.dataservices

import com.singerinstruments.tcats.data.WorkflowDataList

/**
 * Service supporting WorkflowData
 */
public interface WorkflowDataService {
    public fun getWorkflowData() : WorkflowDataList
    public fun addWorkflow(name: String)
}

