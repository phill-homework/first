package com.singerinstruments.tcats.dataservices

import com.singerinstruments.tcats.data.UnitTypeData
import com.singerinstruments.tcats.data.UnitTypeDataList

/**
 * A mechanism for retrieving UnitTypeData
 */
public interface UnitTypeDataService {
    public fun getModuleTypeData(): UnitTypeDataList
    public fun createModuleType(unitTypeData: UnitTypeData)

}