package com.singerinstruments.tcats.dataservices.http

import com.singerinstruments.tcats.data.WorkflowDataList
import com.singerinstruments.tcats.dataservices.BaseAddress
import com.singerinstruments.tcats.dataservices.WorkflowDataService
import org.eclipse.jetty.client.HttpClient

/**
 * Service supporting WorkflowData across Http connection
 */
public class HttpWorkflowDataService(private val httpClient: HttpClient,
                                     private val baseAddress: BaseAddress
                                        ) : WorkflowDataService {

    override fun getWorkflowData() : WorkflowDataList {
        this.httpClient.start() // no-op if started
        val path = this.baseAddress.serverPath + ":" + this.baseAddress.port + WorkflowDataList.logicalPath
        val response = httpClient.GET(path)
        println("HttpWorkflowDataService " + response.contentAsString)
         return WorkflowDataList.fromJson(response.contentAsString)
    }

    override fun addWorkflow(name: String) {
        TODO("Not yet implemented")
    }
}

