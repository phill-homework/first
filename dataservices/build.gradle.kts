
plugins {
    id("com.singerinstruments.tcats.kotlin-library-conventions")
}

dependencies {
    implementation(project(":data"))
    implementation("org.eclipse.jetty", "jetty-client", "11.0.0")
}