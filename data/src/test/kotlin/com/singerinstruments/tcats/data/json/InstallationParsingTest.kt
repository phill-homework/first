package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.ExampleWallData
import com.singerinstruments.tcats.data.WallData
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class InstallationParsingTest {

    @Test
    fun testParseInstalation() {
        val originalData = ExampleWallData.default

        // serialise the original item to json
        val originalJson = originalData.json
        //println(originalJson)
        // de-serialise a new instance from the json
        val newData = WallData.fromJson(originalJson)


        // use new instance to generate new json
        val newJson = newData.json
        //println(newJson)

        // check equality
        Assertions.assertEquals(originalData, newData)

        // original and new json should be identical
        Assertions.assertEquals(originalJson, newJson)
    }
}

