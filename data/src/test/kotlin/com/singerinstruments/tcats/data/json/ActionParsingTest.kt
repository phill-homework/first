package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.ActionDataList
import com.singerinstruments.tcats.data.StandardActions
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ActionParsingTest {

    @Test
    fun testParseScienceActionDataList() {
        val originalData = StandardActions.list

        // serialise the original item to json
        val originalJson = originalData.json
        //println(originalJson)
        // deserialise a new instance from the json
        val newData = ActionDataList.fromJson(originalJson)

        // use new instance to generate new json
        val newJson = newData.json
        //println(newJson)
        // original and new json should be identical
        Assertions.assertEquals(originalJson, newJson)

        // check equality
        Assertions.assertEquals(originalData, newData)
    }
}

