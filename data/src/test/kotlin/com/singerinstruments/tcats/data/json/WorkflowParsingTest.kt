package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.ExampleWorkflowData
import com.singerinstruments.tcats.data.WorkflowDataList
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class WorkflowParsingTest {

    @Test
    fun testParseWorkflowDataList() {
        val originalWorkflowList = ExampleWorkflowData.workflowDataList

        // serialise the original item to json
        val originalJson = originalWorkflowList.json
        //println(originalJson)
        // deserialise a new instance from the json
        val newWorkflowDataList = WorkflowDataList.fromJson(originalJson)

        // check equality
        assertEquals(originalWorkflowList, newWorkflowDataList)

        // use new instance to generate new json
        val newJson = newWorkflowDataList.json

        // original and new json should be identical
        assertEquals(originalJson, newJson)
    }
}

