package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.DataModel
import com.singerinstruments.tcats.data.ExampleWallData
import com.singerinstruments.tcats.data.ExampleWorkflowData
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ModelParsingTest {

    @Test
    fun testParseDataModel() {
        val originalData = DataModel(ExampleWallData.defaultList, ExampleWorkflowData.workflowDataList)

        // serialise the original item to json
        val originalJson = originalData.json
        println(originalJson)
        // deserialise a new instance from the json
        val newData = DataModel.fromJson(originalJson)

        // use new instance to generate new json
        val newJson = newData.json
        println(newJson)
        // original and new json should be identical
        Assertions.assertEquals(originalJson, newJson)

        // check equality
        Assertions.assertEquals(originalData, newData)
    }
}