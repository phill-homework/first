package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.ExampleWallData
import com.singerinstruments.tcats.data.WallDataList
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class InstallationListParsingTest {

    @Test
    fun testParseInstalationList() {
        val originalData = ExampleWallData.defaultList

        // serialise the original item to json
        val originalJson = originalData.json
        println(originalJson)
        // de-serialise a new instance from the json
        val newData = WallDataList.fromJson(originalJson)


        // use new instance to generate new json
        val newJson = newData.json
        println(newJson)

        // check equality
        Assertions.assertEquals(originalData, newData)

        // original and new json should be identical
        Assertions.assertEquals(originalJson, newJson)
    }
}