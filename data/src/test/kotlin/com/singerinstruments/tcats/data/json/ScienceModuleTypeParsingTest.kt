package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.SingerUnitTypeData
import com.singerinstruments.tcats.data.UnitTypeData
import com.singerinstruments.tcats.data.UnitTypeDataList
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ScienceModuleTypeParsingTest {

    @Test
    fun testParseScienceActionDataList() {
        val originalData = SingerUnitTypeData.list

        // serialise the original item to json
        val originalJson = originalData.json
        println(originalJson)
        // deserialise a new instance from the json
        val newData = UnitTypeDataList.fromJson(originalJson)



        // use new instance to generate new json
        val newJson = newData.json
        println(newJson)
        // original and new json should be identical
        Assertions.assertEquals(originalJson, newJson)

        // check equality
        Assertions.assertEquals(originalData, newData)

    }

    @Test
    fun testParseScienceActionData() {
        val originalData = SingerUnitTypeData.list.first()

        // serialise the original item to json
        val originalJson = originalData.json
        println(originalJson)
        // deserialise a new instance from the json
        val newData = UnitTypeData.fromJson(originalJson)



        // use new instance to generate new json
        val newJson = newData.json
        println(newJson)
        // original and new json should be identical
        Assertions.assertEquals(originalJson, newJson)

        // check equality
        Assertions.assertEquals(originalData, newData)

    }
}