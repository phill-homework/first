package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.ExampleUnitData
import com.singerinstruments.tcats.data.UnitData
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ModuleParsingTest {

    @Test
    fun testParseModule() {
        val originalData = ExampleUnitData.store_0_0

        // serialise the original item to json
        val originalJson = originalData.json
        println(originalJson)
        // de-serialise a new instance from the json
        val newData = UnitData.fromJson(originalJson)


        // use new instance to generate new json
        val newJson = newData.json
        println(newJson)

        // check equality
        Assertions.assertEquals(originalData, newData)

        // original and new json should be identical
        Assertions.assertEquals(originalJson, newJson)
    }
}