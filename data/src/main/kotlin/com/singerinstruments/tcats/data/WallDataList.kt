package com.singerinstruments.tcats.data

import com.singerinstruments.tcats.data.json.WallDataListParserDelegate
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.Unparser
import java.io.StringReader
import java.io.StringWriter
import java.io.Writer

public data class WallDataList(
                                        public val installs: MutableList<WallData> = mutableListOf())
                                        : Iterable<WallData> by installs {

    public fun add(data: WallData) {
        // Remove any existing element with the same uid to maintain
        // integrity.
        // This is business logic and should not belong in data layer??
        this.installs.removeIf { it.uid == data.uid}
        this.installs.add((data))
    }

    public fun writeJsonOn(writer: Writer): Unit = this.unparse(Unparser(writer, true))

    internal fun unparse(unparser: Unparser) {
        unparser.beginList()
        this.installs.forEach { it.unparse(unparser) }
        unparser.endList()
    }

    public val json: String get() {
        val writer = StringWriter()
        this.unparse(Unparser(writer, true))
        return writer.toString()
    }

    public companion object {
        public val logicalPath: String = "/wall"

        public fun fromJson(json: String): WallDataList {
            val reader = StringReader(json)
            var data: WallDataList? = null
            val delegate = WallDataListParserDelegate { data = it }
            Parser(reader, delegate).parse()
            return data!!
        }
    }
}