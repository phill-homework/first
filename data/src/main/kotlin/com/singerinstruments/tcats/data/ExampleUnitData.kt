package com.singerinstruments.tcats.data

public object ExampleUnitData {
    public val store_0_0: UnitData = UnitData(
        "Store",
        SingerUnitTypeData.storeModuleType.uid,
        LocationData(0, 0)
    )
    public val image_0_1: UnitData = UnitData(
        "Imaging Module",
        SingerUnitTypeData.imageModuleType.uid,
        LocationData(0, 1)
    )
    public val pick_0_2: UnitData = UnitData(
        "Picking Module",
        SingerUnitTypeData.pickModuleType.uid,
        LocationData(0, 2)
    )
    public val discard_0_3: UnitData = UnitData(
        "Waste Bin",
        SingerUnitTypeData.discardModuleType.uid,
        LocationData(0, 3)
    )
}