package com.singerinstruments.tcats.data

import com.singerinstruments.tcats.data.json.ScienceModuleTypeDataParserDelegate
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.Unparser
import java.io.StringReader
import java.io.StringWriter
import java.util.*

public data class UnitTypeData (public val name: String = "unnamed",
                                public val actionIds: List<String> = emptyList(),
                                public val uid: String = "UnitType "+UUID.randomUUID().toString())  {

    public companion object {
        internal val uidKey get() = "uid"
        internal val nameKey get() = "name"
        internal val actionsKey get() = "actions"

        public fun fromJson(json: String): UnitTypeData {
            val reader = StringReader(json)
            var capability: UnitTypeData? = null
            val delegate = ScienceModuleTypeDataParserDelegate { capability = it }
            Parser(reader, delegate).parse()
            return capability!!
        }
    }

    internal fun unparse(unparser: Unparser) {
        unparser.beginMap()
        unparser.keyValue(UnitTypeData.uidKey, uid)
        unparser.keyValue(UnitTypeData.nameKey, name)
        unparser.beginList(UnitTypeData.actionsKey)
        this.actionIds.forEach { unparser.string(it) }
        unparser.endList()
        unparser.endMap()
    }

    public val json: String get() {
        val writer = StringWriter()
        this.unparse(Unparser(writer, true))
        return writer.toString()
    }
}

