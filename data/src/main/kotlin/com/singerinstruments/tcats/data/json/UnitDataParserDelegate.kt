package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.LocationData
import com.singerinstruments.tcats.data.UnitData
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate

internal class UnitDataParserDelegate(val onParsed: (UnitData)->Unit): ParserDelegate {
    var data = UnitData()

    private var name = ""
    private var typeId = ""
    private var uid = ""
    private var location = LocationData()

    override fun jsonDidParseString(parser: Parser, key: String?, value: String) {
        when (key) {
            UnitData.nameKey -> this.name = value
            UnitData.typeKey -> this.typeId = value
            UnitData.uidKey -> this.uid = value
        }
    }

    override fun jsonWillParseMap(parser: Parser, key: String?) {
        when (key) {
            UnitData.locationKey -> parser.push(LocationDataParserDelegate { this.location = it })
        }
    }

    override fun jsonDidParseMap(parser: Parser, key: String?) {
        parser.popDelegate()
        onParsed.invoke(UnitData(name, typeId, location, uid))
    }
}