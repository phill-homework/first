package com.singerinstruments.tcats.data

import com.singerinstruments.tcats.data.json.UnitTypeDataListParserDelegate
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.Unparser
import java.io.StringReader
import java.io.StringWriter
import java.io.Writer

public data class UnitTypeDataList(
    public val moduleTypeData: MutableList<UnitTypeData> = mutableListOf()
): Iterable<UnitTypeData> by moduleTypeData {

    public companion object {
        public val logicalPath: String = "/unit-type"

        public fun fromJson(json: String): UnitTypeDataList {
            var types = UnitTypeDataList()
            val reader = StringReader(json)
            val delegate = UnitTypeDataListParserDelegate{ types = it }
            Parser(reader, delegate).parse()
            return types
        }
    }

    public fun writeJsonOn(writer: Writer): Unit = this.unparse(Unparser(writer, true))

    internal fun unparse(unparser: Unparser) {
        unparser.beginList()
        this.forEach { it.unparse(unparser) }
        unparser.endList()
    }

    public val json: String get() {
        val writer = StringWriter()
        val unparser = Unparser(writer, true)
        this.unparse(unparser)
        return writer.toString()
    }
}