package com.singerinstruments.tcats.data

import com.singerinstruments.tcats.data.json.ActionDataListParserDelegate
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.Unparser
import java.io.StringReader
import java.io.StringWriter
import java.io.Writer

/**
 * A convenient container for ActionData instances
 * + can retrieve members by uid
 */
public data class ActionDataList(private val actions: MutableList<ActionData> = mutableListOf<ActionData>()) :
    Iterable<ActionData> by actions {

    // capabilities can only be added by the 'aggregate root'
    // it is possible to have duplicate names
    public fun add(name: String): ActionData =
        ActionData(name)
            .also { this.add(it) }

    // capabilities can only be removed by the 'aggregate root'
    // it is possible to have duplicate names
    public fun remove(uid: String): ActionData?
            = this.actions
        .firstOrNull { it.uid == uid }
                ?.also { this.actions.remove(it) }

    public operator fun get(uid: String): ActionData? = this.actions.firstOrNull { it.uid == uid }

    internal fun add(action: ActionData) {
        this.actions.removeIf { action.uid == it.uid }
        this.actions.add(action)
    }

    public fun writeJsonOn(writer: Writer): Unit = this.unparse(Unparser(writer, true))

    internal fun unparse(unparser: Unparser) {
        unparser.beginList()
        this.forEach { it.unparse(unparser) }
        unparser.endList()
    }

    public val json: String get() {
        val writer = StringWriter()
        val unparser = Unparser(writer, true)
        this.unparse(unparser)
        return writer.toString()
    }

    public companion object {
        public val logicalPath: String = "/actions"

        public fun fromJson(json: String): ActionDataList {
            var actions = ActionDataList()
            val reader = StringReader(json)
            val delegate = ActionDataListParserDelegate{ actions = it }
            Parser(reader, delegate).parse()
            return actions
        }
    }
}

