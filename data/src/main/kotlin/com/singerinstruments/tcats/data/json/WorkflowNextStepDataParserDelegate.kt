package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.WorkflowNextStepData
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate

internal class WorkflowNextStepDataParserDelegate(val onParsed: (WorkflowNextStepData)->Unit ): ParserDelegate {
    private var condition = ""
    private var stepId = 0L

    override fun jsonDidParseString(parser: Parser, key: String?, value: String) {
        when (key) {
            WorkflowNextStepData.conditionKey -> condition = value
            WorkflowNextStepData.stepIdKey -> stepId = value.toLong()
        }
    }

    override fun jsonDidParseMap(parser: Parser, key: String?) {
        parser.popDelegate()
        onParsed.invoke(WorkflowNextStepData(stepId, condition))
    }
}