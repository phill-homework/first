package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.ActionDataList
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate

internal class ActionDataListParserDelegate(val onParsed: (ActionDataList)->Unit ): ParserDelegate {
    val actions = ActionDataList()


    override fun jsonWillParseMap(parser: Parser, key: String?) {
        parser.push(ActionDataParserDelegate { actions.add(it) })
    }


    override fun jsonDidParseList(parser: Parser, key: String?) {
        parser.popDelegate()
        onParsed.invoke(actions)
    }
}

