package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.WorkflowData
import com.singerinstruments.tcats.data.WorkflowVersionData
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate



/**
 * Creates a WorkflowData instance from its json representation
 */
internal class WorkflowDataParserDelegate(val onParsed: (WorkflowData)->Unit ): ParserDelegate {
    private var versions = listOf<WorkflowVersionData>()
    private var name = ""
    private var uid = ""

    override fun jsonWillParseList(parser: Parser, key: String?) {
        when (key) {
            WorkflowData.versionsKey ->
                parser.push(WorkflowVersionDataListParserDelegate {
                    this@WorkflowDataParserDelegate.versions = it })
        }
    }

    override fun jsonDidParseString(parser: Parser, key: String?, value: String) {
        when (key) {
            WorkflowData.nameKey -> name = value
            WorkflowData.uidKey -> uid = value
        }
    }

    override fun jsonDidParseMap(parser: Parser, key: String?) {
        parser.popDelegate()
        onParsed.invoke(WorkflowData(name, uid, versions.toMutableList()))
    }
}