package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.*
import uk.vanleersum.kotlin.json.ListOfMapParserDelegate
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate

public class DataModelParserDelegate(private val onParse: (DataModel) -> Unit) : ParserDelegate {
    private var walls: WallDataList = WallDataList()
    private var workflows: WorkflowDataList = WorkflowDataList()
    private var unitTypes: UnitTypeDataList = SingerUnitTypeData.list
    private var actions: ActionDataList = StandardActions.list

    override fun jsonWillParseList(parser: Parser, key: String?) {
        when (key) {
            DataModel.actionsKey -> parser.push(ActionDataListParserDelegate { this.actions = it })
            DataModel.unitTypesKey -> parser.push(UnitTypeDataListParserDelegate { this.unitTypes = it })
            DataModel.workflowsKey -> parser.push(ListOfMapParserDelegate(WorkflowDataParserDelegate {
                workflows.add(
                    it
                )
            }))
            DataModel.wallsKey -> parser.push(ListOfMapParserDelegate(WallDataParserDelegate { this.walls.add(it) }))
        }
    }

    override fun jsonDidParseMap(parser: Parser, key: String?) {
        this.onParse.invoke(DataModel(this.walls, this.workflows, this.unitTypes, this.actions))
    }
}