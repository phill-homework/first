package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.WorkflowData
import com.singerinstruments.tcats.data.WorkflowDataList
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate

/**
 * constructs a list of WorkflowData from its json representation
 */
internal class WorkflowDataListParserDelegate(val onParsed: (WorkflowDataList) -> Unit) : ParserDelegate {
    private val workflows = mutableListOf<WorkflowData>()

    override fun jsonWillParseMap(parser: Parser, key: String?) =
        parser.push(WorkflowDataParserDelegate { workflows.add(it) })

    override fun jsonDidParseList(parser: Parser, key: String?) {
        parser.popDelegate()
        onParsed.invoke(WorkflowDataList(workflows))
    }
}