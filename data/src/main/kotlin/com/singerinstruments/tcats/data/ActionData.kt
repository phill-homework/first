package com.singerinstruments.tcats.data

import com.singerinstruments.tcats.data.json.ActionDataParserDelegate
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.Unparser
import java.io.StringReader
import java.io.StringWriter
import java.util.*

/**
 * Represents a distinct action performed by a science module
 * + has a user friendly name
 * + has a list of property names
 * + has a globally unique id
 */
public data class ActionData(public val name: String,
                             public val properties: List<String> = emptyList<String>(),
                             public val uid: String = "Action " + UUID.randomUUID().toString()) {

    internal fun unparse(unparser: Unparser) {
        unparser.beginMap()
        unparser.keyValue(ActionData.uidKey, uid)
        unparser.keyValue(ActionData.nameKey, name)
        if (this.properties.isNotEmpty()) {
            unparser.beginList(ActionData.propertiesKey)
            this.properties.forEach { unparser.string(it) }
            unparser.endList()
        }
        unparser.endMap()
    }

    public val json: String get() {
        val writer = StringWriter()
        this.unparse(Unparser(writer))
        return writer.toString()
    }

    public companion object {
        internal val uidKey get() = "uid"
        internal val nameKey get() = "name"
        internal val propertiesKey get() = "properties"

        internal fun fromJson(json: String): ActionData {
            val reader = StringReader(json)
            var capability: ActionData? = null
            val delegate = ActionDataParserDelegate { capability = it }
            Parser(reader, delegate).parse()
            return capability!!
        }
    }
}


