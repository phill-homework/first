package com.singerinstruments.tcats.data

import com.singerinstruments.tcats.data.json.WorkflowNextStepDataParserDelegate
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.Unparser
import java.io.StringReader
import java.io.StringWriter

public data class WorkflowNextStepData(public val nextId: Long, public val condition: String) {

    public val json: String get() {
        val writer = StringWriter()
        this.unparse(Unparser(writer))
        return writer.toString()
    }

    internal fun unparse(unparser: Unparser) {
        unparser.beginMap()
        unparser.keyValue(conditionKey, condition)
        unparser.keyValue(stepIdKey, nextId.toString())
        unparser.endMap()
    }

    internal companion object {
        internal val conditionKey: String get() = "condition"
        internal val stepIdKey: String get() = "stepId"

        internal fun fromJson(json: String): WorkflowNextStepData {
            val reader = StringReader(json)
            var data: WorkflowNextStepData? = null
            val delegate = WorkflowNextStepDataParserDelegate { data = it }
            Parser(reader, delegate).parse()
            return data!!
        }
    }
}
