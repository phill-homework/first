package com.singerinstruments.tcats.data

public object SingerUnitTypeData {

    public val storeModuleType: UnitTypeData = UnitTypeData(
        "Store",
        mutableListOf(StandardActions.store.uid),
        "si.type.store"
    )

    public val imageModuleType: UnitTypeData = UnitTypeData(
        "Imager",
        mutableListOf(StandardActions.image.uid),
        "si.type.imager"
    )

    public val pickModuleType: UnitTypeData = UnitTypeData(
        "Picker",
        mutableListOf(StandardActions.pick.uid),
        "si.type.picker"
    )

    public val discardModuleType: UnitTypeData = UnitTypeData(
        "Disposal",
        mutableListOf(StandardActions.discard.uid),
        "si.type.disposal"
    )

    public val phenoboothType: UnitTypeData = UnitTypeData(
        "Phenobooth",
        mutableListOf(StandardActions.image.uid),
        "si.type.phenobooth"
    )

    public val pixlType: UnitTypeData = UnitTypeData(
        "PIXL",
        mutableListOf(StandardActions.image.uid, StandardActions.pick.uid),
        "si.type.pixl"
    )

    public val rotorType: UnitTypeData = UnitTypeData(
        "Rotor",
        mutableListOf(StandardActions.pick.uid),
        "si.type.rotor"
    )

    public val list: UnitTypeDataList = UnitTypeDataList(
        mutableListOf(
            storeModuleType,
            imageModuleType,
            pickModuleType,
            discardModuleType,
            phenoboothType,
            pixlType,
            rotorType,
        )
    )

}

