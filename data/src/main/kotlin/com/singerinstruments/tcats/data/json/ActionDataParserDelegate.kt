package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.ActionData
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate
import uk.vanleersum.kotlin.json.StringListParserDelegate

/**
 * Derives an instance of ActionData from its json representation
 */
internal class ActionDataParserDelegate(val onParsed: (ActionData)->Unit ): ParserDelegate {
    internal var uid = ""
    internal var name = ""
    private val properties = mutableListOf<String>()

    override fun jsonWillParseList(parser: Parser, key: String?) {
        if (ActionData.propertiesKey == key) {
            parser.push(StringListParserDelegate{
                this.properties.add(it)
            })
        }
    }

    override fun jsonDidParseString(parser: Parser, key: String?, value: String) {
        when (key) {
            ActionData.uidKey -> uid = value
            ActionData.nameKey -> name = value
        }
    }

    override fun jsonDidParseMap(parser: Parser, key: String?) {
        parser.popDelegate()
        onParsed.invoke(ActionData(name, this.properties, uid))
    }
}

