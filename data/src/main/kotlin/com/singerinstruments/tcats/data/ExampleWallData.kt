package com.singerinstruments.tcats.data

public object ExampleWallData {
    public val default: WallData = WallData(
        "Default Installation",
        listOf(
            ExampleUnitData.store_0_0,
            ExampleUnitData.image_0_1,
            ExampleUnitData.pick_0_2,
            ExampleUnitData.discard_0_3
        )
    )

    public val simplest: WallData = WallData (
        "Installation",
        listOf(ExampleUnitData.store_0_0, )
    )

    /**
     * create and return an installation referencing this model, but not actually part of it
     */
    public val defaultList: WallDataList = WallDataList(mutableListOf(default))
}