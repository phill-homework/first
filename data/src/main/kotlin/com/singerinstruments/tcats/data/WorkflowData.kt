package com.singerinstruments.tcats.data

import com.singerinstruments.tcats.data.json.WorkflowDataParserDelegate
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.Unparser
import java.io.StringReader
import java.io.StringWriter
import java.util.*

/**
 * Represents a Workflow
 * + has a user-freindly name
 * + has a globally unique id
 * + has a collection of versions
 */
public data class WorkflowData(public val name: String,
                          public val uid: String = UUID.randomUUID().toString(),
                          internal val versions: MutableList<WorkflowVersionData> = mutableListOf())
        : Iterable<WorkflowVersionData> by versions {

    private val lastId: Long get() = this.versions.map { it.version }.maxOrNull() ?: 0L
    private val nextId = this.lastId + 1

    /**
     * Creates a new version of the workflow with the supplied steps.
     */
    public fun setSteps(steps: Iterable<WorkflowStepData>): WorkflowVersionData =
        WorkflowVersionData(this.nextId, steps)
            .also { this@WorkflowData.versions.add(it) }


    public val json: String get() {
        val writer = StringWriter()
        this.unparse(Unparser(writer))
        return writer.toString()
    }

    internal fun unparse(unparser: Unparser) {
        unparser.beginMap()
        unparser.keyValue(WorkflowData.nameKey, name)
        unparser.keyValue(WorkflowData.uidKey, uid)
        unparser.beginList(WorkflowData.versionsKey)
        this.versions.forEach { it.unparse(unparser) }
        unparser.endList()
        unparser.endMap()
    }

    internal companion object {
        internal val nameKey: String get() = "name"
        internal val uidKey: String get() = "uid"
        internal val versionsKey: String get() = "versions"

        internal fun fromJson(json: String): WorkflowData {
            val reader = StringReader(json)
            var data: WorkflowData? = null
            val delegate = WorkflowDataParserDelegate { data = it }
            Parser(reader, delegate).parse()
            return data!!
        }
    }
}
