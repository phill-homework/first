package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.WorkflowStepData
import com.singerinstruments.tcats.data.WorkflowVersionData
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate

/**
 * Creates a WorkflowVersionData instance from its json representation
 */
internal class WorkflowVersionDataParserDelegate(val onParsed: (WorkflowVersionData)->Unit ): ParserDelegate {
    private var stepdata = listOf<WorkflowStepData>()
    private var version = 0L

    override fun jsonWillParseList(parser: Parser, key: String?) {
        when (key) {
            WorkflowVersionData.stepsKey -> parser.push(WorkflowStepDataListParserDelegate {
                this@WorkflowVersionDataParserDelegate.stepdata = it
            })
        }
    }

    override fun jsonDidParseString(parser: Parser, key: String?, value: String) {
        when (key) {
            WorkflowVersionData.versionKey -> version = value.toLong()
        }
    }

    override fun jsonDidParseMap(parser: Parser, key: String?) {
        parser.popDelegate()
        onParsed.invoke(WorkflowVersionData(version, stepdata))
    }
}

