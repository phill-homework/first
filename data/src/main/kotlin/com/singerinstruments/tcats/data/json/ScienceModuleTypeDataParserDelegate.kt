package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.UnitTypeData
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate
import uk.vanleersum.kotlin.json.StringListParserDelegate
import uk.vanleersum.kotlin.json.Unparser
import java.io.StringReader
import java.io.StringWriter


internal class ScienceModuleTypeDataParserDelegate(val onParsed: (UnitTypeData)->Unit ): ParserDelegate {
    internal var uid = ""
    internal var name = ""
    internal val ids = mutableListOf<String>()

    override fun jsonWillParseList(parser: Parser, key: String?) {
        when (key) {
            UnitTypeData.actionsKey -> parser.push(StringListParserDelegate{ ids.add(it) })
        }
    }

    override fun jsonDidParseString(parser: Parser, key: String?, value: String) {
        when (key) {
            UnitTypeData.uidKey -> uid = value
            UnitTypeData.nameKey -> name = value
        }
    }

    override fun jsonDidParseMap(parser: Parser, key: String?) {
        parser.popDelegate()
        onParsed.invoke(UnitTypeData(name, ids, uid))
    }
}
