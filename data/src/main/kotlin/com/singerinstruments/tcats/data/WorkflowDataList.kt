package com.singerinstruments.tcats.data

import com.singerinstruments.tcats.data.json.WorkflowDataListParserDelegate
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.Unparser
import java.io.StringReader
import java.io.StringWriter
import java.io.Writer

/**
 * A list of WorkflowData instances
 */
public data class WorkflowDataList(
    internal val workflows: MutableList<WorkflowData> = mutableListOf())
        : Iterable<WorkflowData> by workflows {

    public fun add(workflowData: WorkflowData) {
        this.workflows.removeIf { workflowData.uid.equals(it.uid) }
        this.workflows.add(workflowData)
    }

    public val json: String get() {
        val writer = StringWriter()
        this.unparse(Unparser(writer, true))
        return writer.toString()
    }

    internal fun unparse(unparser: Unparser) {
        unparser.beginList()
        this.workflows.forEach { it.unparse(unparser) }
        unparser.endList()
    }

    public fun writeJsonOn(writer: Writer): Unit = this.unparse(Unparser(writer, true))

    public companion object {
        public val logicalPath: String = "/workflow"

        public fun fromJson(json: String): WorkflowDataList {
            var workflows = WorkflowDataList()
            val reader = StringReader(json)
            val delegate = WorkflowDataListParserDelegate { workflows = it }
            Parser(reader, delegate).parse()
            return workflows
        }
    }
}

