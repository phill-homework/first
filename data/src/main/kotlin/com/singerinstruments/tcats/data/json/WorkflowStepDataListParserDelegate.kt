package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.WorkflowStepData
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate

/**
 * constructs a list of WorkflowStepData from its json representation
 */
internal class WorkflowStepDataListParserDelegate(val onParsed: (List<WorkflowStepData>)->Unit) : ParserDelegate {
    private val steps = mutableListOf<WorkflowStepData>()

    override fun jsonWillParseMap(parser: Parser, key: String?) =
        parser.push(WorkflowStepDataParserDelegate { steps.add(it) })


    override fun jsonDidParseList(parser: Parser, key: String?) {
        parser.popDelegate()
        onParsed.invoke(steps)
    }
}

