package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.WorkflowNextStepData
import com.singerinstruments.tcats.data.WorkflowStepData
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate

internal class WorkflowStepDataParserDelegate(val onParsed: (WorkflowStepData)->Unit ): ParserDelegate {
    private var uid = ""
    private var id = 0L
    private var nextSteps = listOf<WorkflowNextStepData>()

    override fun jsonDidParseString(parser: Parser, key: String?, value: String) {
        when (key) {
            WorkflowStepData.uidKey -> uid = value
            WorkflowStepData.idKey -> id = value.toLong()
        }
    }

    override fun jsonWillParseList(parser: Parser, key: String?) {
        when (key) {
            WorkflowStepData.nextKey -> parser.push(WorkflowNextStepDataListParserDelegate{nextSteps = it})
        }
    }

    override fun jsonDidParseMap(parser: Parser, key: String?) {
        parser.popDelegate()
        onParsed.invoke(WorkflowStepData(id, uid, nextSteps))
    }
}

