package com.singerinstruments.tcats.data

import com.singerinstruments.tcats.data.json.LocationDataParserDelegate
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.Unparser
import java.io.StringReader
import java.io.StringWriter

public data class LocationData(public val row: Int = 0, public val column: Int = 0) {

    internal fun unparse(unparser: Unparser) {
        unparser.beginMap()
        unparser.keyValue(rowKey, this.row.toString())
        unparser.keyValue(columnKey, this.column.toString())
        unparser.endMap()
    }

    public val json: String get() {
        val writer = StringWriter()
        this.unparse(Unparser(writer, true))
        return writer.toString()
    }

    internal companion object {
        internal val rowKey get() = "row"
        internal val columnKey get() = "column"

        internal fun fromJson(json: String): LocationData {
            val reader = StringReader(json)
            var locationData: LocationData? = null
            val delegate = LocationDataParserDelegate { locationData = it }
            Parser(reader, delegate).parse()
            return locationData!!
        }
    }
}