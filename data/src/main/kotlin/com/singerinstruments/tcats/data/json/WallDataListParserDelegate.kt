package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.WallDataList
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate

internal class WallDataListParserDelegate(val onParsed: (WallDataList)->Unit):
    ParserDelegate {
    var data = WallDataList()


    override fun jsonWillParseMap(parser: Parser, key: String?) {
        parser.push(WallDataParserDelegate { this.data.add(it) })
    }

    override fun jsonDidParseList(parser: Parser, key: String?) {
        parser.popDelegate()
        onParsed.invoke(data)
    }
}