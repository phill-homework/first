package com.singerinstruments.tcats.data

import com.singerinstruments.tcats.data.json.WorkflowVersionDataParserDelegate
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.Unparser
import java.io.StringReader
import java.io.StringWriter

/**
 * A version of a workflow
 * + has a unique version number within its containing workflow
 * + has a collection of WorkflowStepData
 */
public data class WorkflowVersionData(public val version: Long,
                                      public val steps: Iterable<WorkflowStepData>) {

    public val json: String get() {
        val writer = StringWriter()
        this.unparse(Unparser(writer))
        return writer.toString()
    }

    internal fun unparse(unparser: Unparser) {
        unparser.beginMap()
        unparser.keyValue(WorkflowVersionData.versionKey, version.toString())
        unparser.beginList(WorkflowVersionData.stepsKey)
        this.steps.forEach {it.unparse(unparser)}
        unparser.endList()
        unparser.endMap()
    }

    internal companion object {
        internal val versionKey: String get() = "version"
        internal val stepsKey: String get() = "steps"

        internal fun fromJson(json: String): WorkflowVersionData {
            val reader = StringReader(json)
            var data: WorkflowVersionData? = null
            val delegate = WorkflowVersionDataParserDelegate { data = it }
            Parser(reader, delegate).parse()
            return data!!
        }
    }
}
