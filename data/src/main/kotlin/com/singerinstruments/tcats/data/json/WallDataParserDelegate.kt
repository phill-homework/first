package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.WallData
import com.singerinstruments.tcats.data.UnitData
import uk.vanleersum.kotlin.json.ListOfMapParserDelegate
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate

internal class WallDataParserDelegate(val onParsed: (WallData)->Unit): ParserDelegate {
    var data = WallData()

    private var name = ""
    private var uid = ""
    private var modules = mutableListOf<UnitData>()

    override fun jsonDidParseString(parser: Parser, key: String?, value: String) {
        when (key) {
            WallData.nameKey -> this.name = value
            WallData.uidKey -> this.uid = value
        }
    }

    override fun jsonWillParseList(parser: Parser, key: String?) {
        parser.push(ListOfMapParserDelegate(UnitDataParserDelegate { this.modules.add(it) }))
    }

    override fun jsonDidParseMap(parser: Parser, key: String?) {
        parser.popDelegate()
        onParsed.invoke(WallData(name, modules, uid))
    }
}

