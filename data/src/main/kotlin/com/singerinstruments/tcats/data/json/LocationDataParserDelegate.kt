package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.LocationData
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate

internal class LocationDataParserDelegate(val onParsed: (LocationData)->Unit): ParserDelegate {
    var row = 0
    var column = 0

    override fun jsonDidParseString(parser: Parser, key: String?, value: String) {
        when (key) {
            LocationData.rowKey -> row = value.toInt()
            LocationData.columnKey -> column = value.toInt()
        }
    }

    override fun jsonDidParseMap(parser: Parser, key: String?) {
        parser.popDelegate()
        onParsed.invoke(LocationData(row, column))
    }
}

