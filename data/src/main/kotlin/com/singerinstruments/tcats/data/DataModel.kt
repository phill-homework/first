package com.singerinstruments.tcats.data

import com.singerinstruments.tcats.data.json.DataModelParserDelegate
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.Unparser
import java.io.*

public data class DataModel(
    public val walls: WallDataList = WallDataList(),
    public val workflows: WorkflowDataList = WorkflowDataList(),
    public val unitTypes: UnitTypeDataList = SingerUnitTypeData.list,
    public val actions: ActionDataList = StandardActions.list
) {
    public val json: String get() = json(this)

    public companion object {
        public fun from(file: File): DataModel {
            var model: DataModel? = null
            FileReader(file).use { reader ->
                Parser(reader, DataModelParserDelegate { model = it }).parse()
            }
            return model!!
        }

        public fun fromJson(json: String): DataModel {
            var model: DataModel? = null
            Parser(StringReader(json), DataModelParserDelegate { model = it }).parse()
            return model!!
        }

        internal val actionsKey = "actions"
        internal val unitTypesKey = "unitTypes"
        internal val wallsKey = "walls"
        internal val workflowsKey = "workflows"

        internal fun unparse(model: DataModel, unparser: Unparser) {
            unparser.beginMap()
            unparser.key(actionsKey)
            model.actions.unparse(unparser)
            unparser.key(unitTypesKey)
            model.unitTypes.unparse(unparser)
            unparser.key(wallsKey)
            model.walls.unparse(unparser)
            unparser.key(workflowsKey)
            model.workflows.unparse(unparser)
            unparser.endMap()
        }

        public fun write(model: DataModel, file: File) {
            FileWriter(file).use {
                unparse(model, Unparser(it, true))
            }
        }

        public fun json(model: DataModel): String {
            val writer = StringWriter()
            unparse(model, Unparser(writer, true))
            return writer.toString()
        }
    }
}

