package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.UnitTypeData
import com.singerinstruments.tcats.data.UnitTypeDataList
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate

internal class UnitTypeDataListParserDelegate(val onParsed: (UnitTypeDataList)->Unit ):
    ParserDelegate {
    val types = mutableListOf<UnitTypeData>()

    // We will parse a list of maps.  We can ignore the start and end of the list and just react to
    // encountering a map with a null key.  Note that this lazy strategy will break if we have multiple
    // lists...
    override fun jsonWillParseMap(parser: Parser, key: String?) {
        parser.push(ScienceModuleTypeDataParserDelegate { types.add(it) })
    }

    override fun jsonDidParseList(parser: Parser, key: String?) {
        parser.popDelegate()
        onParsed.invoke(UnitTypeDataList(types))
    }
}