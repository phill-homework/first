package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.WorkflowNextStepData
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate

internal class WorkflowNextStepDataListParserDelegate(val onParsed: (List<WorkflowNextStepData>)->Unit) :
    ParserDelegate {
    private val steps = mutableListOf<WorkflowNextStepData>()

    override fun jsonWillParseMap(parser: Parser, key: String?) =
        parser.push(WorkflowNextStepDataParserDelegate { steps.add(it) })


    override fun jsonDidParseList(parser: Parser, key: String?) {
        parser.popDelegate()
        onParsed.invoke(steps)
    }
}