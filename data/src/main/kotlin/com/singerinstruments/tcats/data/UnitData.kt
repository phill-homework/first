package com.singerinstruments.tcats.data

import com.singerinstruments.tcats.data.json.UnitDataParserDelegate
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.Unparser
import java.io.StringReader
import java.io.StringWriter
import java.util.*

public data class UnitData (public val name: String = "unnamed",
                            public val moduleTypeId: String = "",
                            public val locationData: LocationData = LocationData(),
                            public val uid: String = "Unit "+UUID.randomUUID().toString())  {

    internal fun unparse(unparser: Unparser) {
        unparser.beginMap()
        unparser.keyValue(UnitData.nameKey, this.name)
        unparser.keyValue(UnitData.typeKey, this.moduleTypeId)
        unparser.keyValue(UnitData.uidKey, this.uid)
        unparser.key(UnitData.locationKey)
        this.locationData.unparse(unparser)
        unparser.endMap()
    }

    public val json: String get() {
        val writer = StringWriter()
        this.unparse(Unparser(writer, true))
        return writer.toString()
    }

    public companion object {
        internal val nameKey get() = "name"
        internal val typeKey get() = "type"
        internal val locationKey get() = "location"
        internal val uidKey get() = "uid"

        internal fun fromJson(json: String): UnitData {
            val reader = StringReader(json)
            var data: UnitData? = null
            val delegate = UnitDataParserDelegate { data = it }
            Parser(reader, delegate).parse()
            return data!!
        }
    }
}