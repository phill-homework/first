package com.singerinstruments.tcats.data

public object ExampleWorkflowData {

    public val emptyWorkflowData: WorkflowData = WorkflowData("empty")

    public val workflowNextStepData2Always: WorkflowNextStepData = WorkflowNextStepData(2, "always")
    public val workflowNextStepData3Success: WorkflowNextStepData = WorkflowNextStepData(3, "success")
    public val workflowNextStepData4Fail: WorkflowNextStepData = WorkflowNextStepData(4, "failure")
    public val workflowNextStepData4Always: WorkflowNextStepData = WorkflowNextStepData(4, "always")

    public val storeWorkflowStepData: WorkflowStepData = WorkflowStepData(
        1, StandardActions.store.uid,
        listOf(workflowNextStepData2Always)
    )
    public val imageWorkflowStepData: WorkflowStepData = WorkflowStepData(
        2, StandardActions.image.uid,
        listOf(workflowNextStepData3Success, workflowNextStepData4Fail)
    )
    public val pickWorkflowStepData: WorkflowStepData = WorkflowStepData(
        3, StandardActions.pick.uid,
        listOf(workflowNextStepData4Always)
    )
    public val discardWorkflowStepData: WorkflowStepData = WorkflowStepData(4, StandardActions.discard.uid)


    public val workflowVersionData: WorkflowVersionData =
        WorkflowVersionData(
            1,
            listOf(storeWorkflowStepData, imageWorkflowStepData, pickWorkflowStepData, discardWorkflowStepData)
        )

    public val workflowData: WorkflowData = WorkflowData("simple", versions = mutableListOf(workflowVersionData))

    public val workflowDataList: WorkflowDataList = WorkflowDataList(mutableListOf(workflowData))
}