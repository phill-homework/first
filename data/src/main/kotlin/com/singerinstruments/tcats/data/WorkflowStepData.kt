package com.singerinstruments.tcats.data

import com.singerinstruments.tcats.data.json.WorkflowStepDataParserDelegate
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.Unparser
import java.io.StringReader
import java.io.StringWriter

/**
 * Represents a step in a workflow.
 * + Has a distinct id unique within the workflow
 * + has a reference to an action
 * + will have a reference to the next step(s)
 */
public data class WorkflowStepData(
    public val id: Long,
    public val actionId: String,
    public val next: Iterable<WorkflowNextStepData> = emptyList()
) {

    public val json: String get() {
        val writer = StringWriter()
        this.unparse(Unparser(writer))
        return writer.toString()
    }

    internal fun unparse(unparser: Unparser) {
        unparser.beginMap()
        unparser.keyValue(WorkflowStepData.idKey, id.toString())
        unparser.keyValue(WorkflowStepData.uidKey, actionId)
        unparser.beginList(WorkflowStepData.nextKey)
        this.next.forEach { it.unparse(unparser) }
        unparser.endList()
        unparser.endMap()
    }

    internal companion object {
        internal val uidKey: String get() = "actionId"
        internal val idKey: String get() = "id"
        internal val nextKey: String get() = "next"

        internal fun fromJson(json: String): WorkflowStepData {
            val reader = StringReader(json)
            var data: WorkflowStepData? = null
            val delegate = WorkflowStepDataParserDelegate { data = it }
            Parser(reader, delegate).parse()
            return data!!
        }
    }
}

