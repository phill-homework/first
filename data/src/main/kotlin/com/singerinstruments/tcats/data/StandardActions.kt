package com.singerinstruments.tcats.data

public object StandardActions {
    public val none: ActionData = ActionData("none", emptyList(), "si.action.none")
    public val image: ActionData = ActionData("Image", emptyList(), "si.action.image")
    public val pick: ActionData = ActionData("Pick", emptyList(), "si.action.pick")
    public val read: ActionData = ActionData("Read", emptyList(), "si.action.read")
    public val seal: ActionData = ActionData("Seal", emptyList(), "si.action.seal")
    public val centriguge: ActionData = ActionData("Centriguge", listOf("duration"), "si.action.centrifuge")
    public val thermocycle: ActionData = ActionData("ThermoCycle", listOf("temperature"), "si.action.thermocycle")
    public val electrophoresis: ActionData = ActionData("Electrophoresis", emptyList(), "si.action.electrophoresis")
    public val liquidhandler: ActionData = ActionData("LiquidHandler", emptyList(), "si.action.liquidhandler")
    public val store: ActionData = ActionData("Store", emptyList(), "si.action.store")
    public val incubate37: ActionData = ActionData("Incubate@37", listOf("duration"), "si.action.incubate37")
    public val incubate30: ActionData = ActionData("Incubate@30", listOf("duration"), "si.action.incubate30")
    public val store4: ActionData = ActionData("Store@4", listOf("duration"), "si.action.store4")
    public val storeMinus20: ActionData = ActionData("Store@-20", listOf("duration"), "si.action.store-20")
    public val storeMinus80: ActionData = ActionData("Store@-80", listOf("duration"), "si.action.store-80")
    public val elevator: ActionData = ActionData("Elevator", emptyList(), "si.action.elevator")
    public val discard: ActionData = ActionData("Discard", emptyList(), "si.action.discard")

    public val all: List<ActionData> = listOf(
        none, image, pick, read, seal, centriguge, thermocycle, electrophoresis, liquidhandler,
        store, incubate37, incubate30, store4, storeMinus20, storeMinus80, elevator, discard
    )

    public val list: ActionDataList = ActionDataList()
        .apply { this@StandardActions.all.forEach { add(it) } }
}