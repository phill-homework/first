package com.singerinstruments.tcats.data.json

import com.singerinstruments.tcats.data.WorkflowVersionData
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate

/**
 * constructs a list of WorkflowVersionData from its json representation
 */
internal class WorkflowVersionDataListParserDelegate(val onParsed: (List<WorkflowVersionData>)->Unit) : ParserDelegate {
    private val versions = mutableListOf<WorkflowVersionData>()

    override fun jsonWillParseMap(parser: Parser, key: String?)
        = parser.push(WorkflowVersionDataParserDelegate { versions.add(it) })


    override fun jsonDidParseList(parser: Parser, key: String?) {
        parser.popDelegate()
        onParsed.invoke(versions)
    }
}