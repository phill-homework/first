package com.singerinstruments.tcats.data

import com.singerinstruments.tcats.data.json.WallDataParserDelegate
import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.Unparser
import java.io.StringReader
import java.io.StringWriter
import java.util.*

public data class WallData(
    public val name: String = "unnamed",
    public val modules: List<UnitData> = emptyList(),
    public val uid: String = "TechWall " + UUID.randomUUID().toString()
) : Iterable<UnitData> by modules {

    internal fun unparse(unparser: Unparser) {
        unparser.beginMap()
        unparser.keyValue(WallData.nameKey, this.name)
        unparser.keyValue(WallData.uidKey, this.uid)
        unparser.beginList(WallData.modulesKey)
        this.modules.forEach { it.unparse(unparser) }
        unparser.endList()
        unparser.endMap()
    }

    public val json: String get() {
        val writer = StringWriter()
        this.unparse(Unparser(writer, true))
        return writer.toString()
    }

    public companion object {
        internal val nameKey get() = "name"
        internal val modulesKey get() = "modules"
        internal val uidKey get() = "uid"

        public fun fromJson(json: String): WallData {
            val reader = StringReader(json)
            var data: WallData? = null
            val delegate = WallDataParserDelegate { data = it }
            Parser(reader, delegate).parse()
            return data!!
        }
    }
}

