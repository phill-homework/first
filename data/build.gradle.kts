
plugins {
    id("com.singerinstruments.tcats.kotlin-library-conventions")
}

dependencies {
    implementation(project(":json"))
}