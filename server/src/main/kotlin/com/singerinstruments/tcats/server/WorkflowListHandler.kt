package com.singerinstruments.tcats.server

import com.singerinstruments.tcats.dataservices.WorkflowDataService
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.eclipse.jetty.server.Request
import org.eclipse.jetty.server.handler.AbstractHandler

internal class WorkflowListHandler(val dataService: WorkflowDataService) : AbstractHandler() {
    override fun handle(target: String, baseRequest: Request, request: HttpServletRequest, response: HttpServletResponse) {
        baseRequest.setHandled(true)

        when (baseRequest.method) {
            "GET" -> {
                this.dataService.getWorkflowData().writeJsonOn(response.writer)
            }
            "POST" -> {
//                val name = baseRequest.parameterMap["name"]?.firstOrNull()
//                if (null != name) {
//                    this.capabilities.add(name)
//                }
            }
        }
    }
}
