package com.singerinstruments.tcats.server

import com.singerinstruments.tcats.dataservices.ActionDataService
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.eclipse.jetty.server.Request
import org.eclipse.jetty.server.handler.AbstractHandler

internal class ScienceActionHandler(val dataService: ActionDataService) : AbstractHandler() {
    override fun handle(target: String, baseRequest: Request, request: HttpServletRequest, response: HttpServletResponse) {
        baseRequest.isHandled = true

        when (baseRequest.method) {
            "GET" -> {
                this.dataService.getActionData().writeJsonOn(response.writer)
            }
            "POST" -> {
                val name = baseRequest.parameterMap["name"]?.firstOrNull()
                if (null != name) {
                    this.dataService.addAction(name)
                }
            }
        }
    }
}


