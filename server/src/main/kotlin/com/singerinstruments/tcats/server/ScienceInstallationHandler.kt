package com.singerinstruments.tcats.server

import com.singerinstruments.tcats.data.WallData
import com.singerinstruments.tcats.dataservices.WallDataService
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.eclipse.jetty.server.Request
import org.eclipse.jetty.server.handler.AbstractHandler
import java.util.stream.Collectors

internal class ScienceInstallationHandler(val dataService: WallDataService) : AbstractHandler() {
    override fun handle(target: String, baseRequest: Request, request: HttpServletRequest, response: HttpServletResponse) {
        baseRequest.isHandled = true

        when (baseRequest.method) {
            "GET" -> {
                this.dataService.getDataList().writeJsonOn(response.writer)
            }
            "POST" -> {
                val l = request.reader.lines().collect(Collectors.joining(System.lineSeparator()))
                //println("ScienceModuleTypeListHandler $l")
                val d = WallData.fromJson(l)
                dataService.putWallData(d)
            }
        }
    }
}