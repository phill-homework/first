package com.singerinstruments.tcats.server

import com.singerinstruments.tcats.data.ActionDataList
import com.singerinstruments.tcats.data.UnitTypeDataList
import com.singerinstruments.tcats.data.WallDataList
import com.singerinstruments.tcats.data.WorkflowDataList
import com.singerinstruments.tcats.dataservices.DataService
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.ServerConnector
import org.eclipse.jetty.server.handler.ContextHandler
import org.eclipse.jetty.server.handler.ContextHandlerCollection
import org.eclipse.jetty.util.thread.QueuedThreadPool
import java.net.InetAddress


class TCatServer(initialPort: Int = 8081, service: DataService) {

    private val threadPool = QueuedThreadPool()
    private val server = Server(threadPool)
    private val connector = ServerConnector(server)


    init {
        threadPool.name = "tcats-server"
        connector.port = initialPort
        server.addConnector(connector)

        val contextCollection = ContextHandlerCollection()

        contextCollection.addHandler(
            ContextHandler(ActionDataList.logicalPath)
                .apply { handler = ScienceActionHandler(service.actionDataService) })

        contextCollection.addHandler(
            ContextHandler(UnitTypeDataList.logicalPath)
                .apply { handler = ScienceModuleTypeListHandler(service.unitTypeDataService) })

        contextCollection.addHandler(
            ContextHandler(WorkflowDataList.logicalPath)
                .apply { handler = WorkflowListHandler(service.workflowDataService) })

        contextCollection.addHandler(
            ContextHandler(WallDataList.logicalPath)
                .apply { handler = ScienceInstallationHandler(service.wallDataService) })


        server.handler = contextCollection
    }

    fun addressString(): String  {
        val addr = InetAddress.getLocalHost()
        val hostname: String = addr.hostName
        val port = connector.localPort
        return hostname + ":" + port
    }

    internal fun start() {
        server.start()
    }

    internal fun stop() {
        server.stop()
    }
}