package com.singerinstruments.tcats.server

import com.singerinstruments.tcats.data.ExampleWallData
import com.singerinstruments.tcats.data.ExampleWorkflowData
import com.singerinstruments.tcats.dataservices.ephemeral.EphemeralDataService
import java.awt.BorderLayout
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import javax.swing.JFrame
import javax.swing.JMenu
import javax.swing.JMenuBar
import javax.swing.JMenuItem

//https://www.eclipse.org/jetty/documentation/jetty-11/programming_guide.php
object TCatsServerFrame {

    @JvmStatic
    @Suppress("UNUSED_PARAMETER")
    // 'args' needs to be there for the correct signature for invocation

    fun main(args : Array<String>) {
        val service = EphemeralDataService(
            wallDataList = ExampleWallData.defaultList,
            workflowDataList = ExampleWorkflowData.workflowDataList
        )

        val server = TCatServer(8081, service)


        val frame = JFrame("TCats Server")
        frame.layout = BorderLayout()

        val menuBar = JMenuBar()
        val menuFile = JMenu("File")

        val menuItemExit = JMenuItem("Exit")
        menuItemExit.addActionListener {
            server.stop()
            System.exit(0)
        }
        menuFile.add(menuItemExit)

        menuBar.add(menuFile)
        frame.jMenuBar = menuBar
        frame.defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE
        frame.setSize(300, 200)

        frame.addWindowListener( object: WindowAdapter() {
            override fun windowOpened(e: WindowEvent?) {
                server.start()
                frame.title = "TCats Server " + server.addressString()
            }

            override fun windowClosing(e: WindowEvent?) {
                server.stop()
            }
        })

        frame.isVisible = true
    }
}