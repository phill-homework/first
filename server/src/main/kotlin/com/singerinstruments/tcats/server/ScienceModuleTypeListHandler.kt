package com.singerinstruments.tcats.server

import com.singerinstruments.tcats.data.UnitTypeData
import com.singerinstruments.tcats.dataservices.UnitTypeDataService
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.eclipse.jetty.server.Request
import org.eclipse.jetty.server.handler.AbstractHandler
import java.util.stream.Collectors

internal class ScienceModuleTypeListHandler(val dataService: UnitTypeDataService) : AbstractHandler() {
    override fun handle(target: String, baseRequest: Request, request: HttpServletRequest, response: HttpServletResponse) {
        baseRequest.isHandled = true

        when (baseRequest.method) {
            "GET" -> {
                this.dataService.getModuleTypeData().writeJsonOn(response.writer)
            }
            "POST" -> {
                //println("SERVER POST")
                val l = request.reader.lines().collect(Collectors.joining(System.lineSeparator()))
                //println("ScienceModuleTypeListHandler $l")
                val d = UnitTypeData.fromJson(l)
                dataService.createModuleType(d)
            }
        }
    }
}