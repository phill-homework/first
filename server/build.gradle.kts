

plugins {
    id("com.singerinstruments.tcats.kotlin-application-conventions")
    id("com.github.johnrengelman.shadow") version "6.1.0"
}

dependencies {
    implementation("org.apache.commons:commons-text")
    implementation("org.eclipse.jetty", "jetty-server", "11.0.0")

}
dependencies {
    implementation(project(":data"))
    implementation(project(":dataservices"))
}


application {
    // https://github.com/johnrengelman/shadow/issues/336
    @Suppress("DEPRECATION")
    mainClassName ="com.singerinstruments.tcats.server.TCatsServerFrame"
    mainClass.set("com.singerinstruments.tcats.server.TCatsServerFrame")
}

