/*
 * Copyright (c) 2019. Phill van Leersum
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.vanleersum.kotlin.json

import java.io.IOException
import java.io.Reader

/**
 *
 * @author phill
 */
public class Parser(private val reader: Reader, parserDelegate: ParserDelegate) {

    private val EOF = -1
    private val controlCodesAsInts = Json.values().map{it.int}

    // Delegate stack.  This will npe if misused by popping too many delegates.
    private val delegates = mutableListOf<ParserDelegate>(DummyDelegate())
    private val currentDelegate get() = this.delegates.last()

    init {this.push(parserDelegate)}

    public fun push(parserDelegate: ParserDelegate) {
        this.delegates.add(parserDelegate)
    }

    public fun popDelegate(): ParserDelegate? {
        val delegate = this.delegates.removeLastOrNull()
        if (null != delegate) {
            this.currentDelegate.jsonDidPopDelegate(this, delegate)
        }
        return delegate
    }

    @Throws(IOException::class)
    public fun parse() {
        this.readUnknown(EOF, null)
    }

    /* return the next token start character. The character will have been removed
	 * from the stream, and will be returned. It will be '"', '[', ']', '{', or '}'
	 */
    @Throws(IOException::class)
    private fun nextControlCharacter(): Int {
        // read stream until we get '[', '{' or '"'
        var c = this.reader.read()
        while (EOF != c && !this.controlCodesAsInts.contains(c)) {
            c = this.reader.read()
        }
        return c
    }

    /* Read a string representing a map key from the stream. The initial '"' must
	 * have already been consumed. The entire string including the terminal '"' will
	 * be removed from the stream and returned. Escaped characters will be correctly
	 * handled
	 */
    @Throws(IOException::class)
    private fun readMapKey(): String? {
        val nextControlCharacter = this.nextControlCharacter()
        if (Json.MapEnd.int == nextControlCharacter) {
            return null
        } else if (Json.StringDelimiter.int == nextControlCharacter) {
            return this.readString()
        }
        throw IOException("Unexpected character: $nextControlCharacter")
    }

    /* Read a string from the stream. The initial '"' must have already been
	 * consumed. The entire string including the terminal '"' will be removed from
	 * the stream and returned. Escaped characters will be correctly handled
	 */
    @Throws(IOException::class)
    private fun readString(): String {
        val buf = StringBuilder()
        var c = this.reader.read()
        while (Json.StringDelimiter.int != c) {
            if ('\\'.toInt() == c) {
                c = this.reader.read()
            }
            buf.append(c.toChar())
            c = this.reader.read()
        }
        return buf.toString()
    }

    // used to extract a list, map or string. Looks for a control character that
    // matches the start of
    // a string, list or map and parses for that entity.
    // if the expected terminator is found then the method returns false
    @Throws(IOException::class)
    private fun readUnknown(expectedTerminator: Int, key: String?): Boolean {
        val nextControlCharacter = this.nextControlCharacter()
        if (expectedTerminator == nextControlCharacter) {
            return false
        }
        when (nextControlCharacter) {
            Json.StringDelimiter.int -> {
                val value = this.readString()
                this.currentDelegate.jsonDidParseString(this, key, value)
            }
            Json.ListStart.int -> {
                this.currentDelegate.jsonWillParseList(this, key)
                while (this.readUnknown(Json.ListEnd.int, null)) {
                    // Intentionally empty
                }
                this.currentDelegate.jsonDidParseList(this, key)
            }
            Json.MapStart.int -> {
                this.currentDelegate.jsonWillParseMap(this, key)
                var mapKey = this.readMapKey()
                while (null != mapKey) {
                    this.readUnknown(EOF, mapKey)
                    mapKey = this.readMapKey()
                }
                this.currentDelegate.jsonDidParseMap(this, key)
            }
            else -> throw IOException("Unexpected character: $nextControlCharacter")
        }
        return true
    }
}
