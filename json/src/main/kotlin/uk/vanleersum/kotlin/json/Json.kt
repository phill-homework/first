/*
 * Copyright (c) 2019. Phill van Leersum
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.vanleersum.kotlin.json

internal enum class Json(val char: Char) {
    StringDelimiter('"'),
    MapEnd('}'),
    MapStart('{'),
    ListEnd(']'),
    ListStart('[')
    ;

    val int: Int get() = char.toInt()
    val string: String get() = char.toString()

    val values by lazy {arrayOf(StringDelimiter,MapEnd,MapStart,ListEnd,ListStart)}
}