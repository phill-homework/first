package uk.vanleersum.kotlin.json

internal enum class UnparserState {
    Empty, InList, InMapExpectingKey, InMapExpectingValue
}