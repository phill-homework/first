package uk.vanleersum.kotlin.json

import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate

public class StringListParserDelegate(public val onParsedString: (String)->Unit) : ParserDelegate {

    override fun jsonDidParseString(parser: Parser, key: String?, value: String) {
        onParsedString.invoke(value)
    }

    override fun jsonDidParseList(parser: Parser, key: String?) {
        parser.popDelegate()
    }
}