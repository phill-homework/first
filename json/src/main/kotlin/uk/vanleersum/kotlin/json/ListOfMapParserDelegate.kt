package uk.vanleersum.kotlin.json

import uk.vanleersum.kotlin.json.Parser
import uk.vanleersum.kotlin.json.ParserDelegate

public class ListOfMapParserDelegate(private val mapParser: ParserDelegate): ParserDelegate {

    override fun jsonWillParseMap(parser: Parser, key: String?) {
        parser.push(mapParser)
    }

    override fun jsonDidParseList(parser: Parser, key: String?) {
        parser.popDelegate()
    }
}