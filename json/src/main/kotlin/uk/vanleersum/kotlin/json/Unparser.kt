/*
 * Copyright (c) 2019. Phill van Leersum
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.vanleersum.kotlin.json

import java.io.IOException
import java.io.Writer
import java.util.*


public class Unparser(private val writer: Writer, public var pretty: Boolean = false) {
    private val stateStack = ArrayList<UnparserState>()
    private var indent = 0
    //public var pretty: Boolean = false
    private var keyValueSeparator = ":"
    private var listElementSeparator = ","

    init {
        this.push(UnparserState.Empty)
    }

    @Throws(UnparserException::class, IOException::class)
    public fun beginList(key: String? = null) {
        if (null != key) {
            key(key)
        }
        if (this.state() == UnparserState.InMapExpectingKey) {
            throw UnparserException("Expecting a map key - not a list")
        }
        if (this.state() != UnparserState.InMapExpectingValue) {
            this.writeIndent()
        }
        this.writer.write(Json.ListStart.string)
        this.indent(+1)
        this.newline()
        if (this.state() == UnparserState.InMapExpectingValue) {
            this.pop()
        }
        this.push(UnparserState.InList)
    }

    @Throws(UnparserException::class, IOException::class)
    public fun beginMap(key: String? = null) {
        if (null != key) {
            key(key)
        }
        if (this.state() == UnparserState.InMapExpectingKey) {
            throw UnparserException("Expecting a map key - not a map")
        }
        if (this.state() != UnparserState.InMapExpectingValue) {
            this.writeIndent()
        }
        if (this.state() == UnparserState.InMapExpectingValue) {
            this.pop()
        }
        this.writer.write(Json.MapStart.string)
        this.indent(+1)
        this.newline()
        this.push(UnparserState.InMapExpectingKey)
    }

    @Throws(UnparserException::class, IOException::class)
    public fun endList() {
        when (this.state()) {
            UnparserState.Empty -> throw UnparserException("Empty - not expecting end of a list")
            // case InList: throw new UnparserException("In a list - not expecting end of a map");
            UnparserState.InMapExpectingValue -> throw UnparserException("Expecting a map value not end of a list")
            UnparserState.InMapExpectingKey -> throw UnparserException("In a map - not end of a list")
            else -> {
            }
        }
        this.indent(-1)
        this.writeIndent()
        this.writer.write(Json.ListEnd.string)
        this.newline()
        this.pop()
    }

    @Throws(UnparserException::class, IOException::class)
    public fun endMap() {
        when (this.state()) {
            UnparserState.Empty -> throw UnparserException("Empty - not expecting end of a map")
            UnparserState.InList -> throw UnparserException("In a list - not expecting end of a map")
            UnparserState.InMapExpectingValue -> throw UnparserException("Expecting a map value not end of a map")
            UnparserState.InMapExpectingKey -> {
            }
        }
        this.indent(-1)
        this.writeIndent()
        this.writer.write(Json.MapEnd.string)
        this.newline()
        this.pop()
    }

    @Throws(UnparserException::class, IOException::class)
    public fun key(key: String) {
        when (this.state()) {
            UnparserState.Empty -> throw UnparserException("Empty - not expecting a key")
            UnparserState.InList -> throw UnparserException("In a list - not expecting a map key")
            UnparserState.InMapExpectingValue -> throw UnparserException("Expecting a map value not a map key")
            else -> {
            }
        }
        this.writeIndent()
        this.writeString(key)
        this.writer.write(this.keyValueSeparator)
        this.push(UnparserState.InMapExpectingValue)
    }

    @Throws(UnparserException::class, IOException::class)
    public fun keyValue(key: String, value: String) {
        when (this.state()) {
            UnparserState.Empty -> throw UnparserException("Empty - not expecting a key,value pair")
            UnparserState.InList -> throw UnparserException("In a list - not expecting a key,value pair")
            UnparserState.InMapExpectingValue -> throw UnparserException("Expecting a map value not a key,value pair")
            else -> {
            }
        }
        this.writeIndent()
        this.writeString(key)
        this.writer.write(this.keyValueSeparator)
        this.writeString(value)
        this.writer.write(this.listElementSeparator)
        this.newline()
    }

    @Throws(UnparserException::class, IOException::class)
    public fun string(value: String) {
        this.string(value, true)
    }

    @Throws(UnparserException::class, IOException::class)
    private fun string(value: String, pretty: Boolean) {
        if (this.state() == UnparserState.InMapExpectingKey) {
            throw UnparserException("Expecting a map key - not a string")
        }
        if (this.state() != UnparserState.InMapExpectingValue && pretty) {
            this.writeIndent()
        }
        this.writeString(value)
        if (this.state() != UnparserState.Empty) {
            this.writer.write(this.listElementSeparator)
            if (pretty) {
                this.newline()
            }
        }
        if (this.state() == UnparserState.InMapExpectingValue) {
            this.pop()
        }
    }

    @Throws(IOException::class)
    private fun indent(indentDelta: Int) {
        this.indent += indentDelta
    }

    @Throws(IOException::class)
    private fun newline() {
        if (this.pretty) {
            this.writer.write("\n")
        }
    }

    private fun pop() = this.stateStack.removeAt(0)
    private fun push(unparserState: UnparserState) = this.stateStack.add(0, unparserState)
    private fun state() = this.stateStack[0]

    @Throws(IOException::class)
    private fun writeIndent() {
        if (this.pretty) {
            for (i in 0 until this.indent) {
                this.writer.write("    ")
            }
        }
    }

    @Throws(IOException::class)
    private fun writeString(string: String) {
        this.writer.write(Json.StringDelimiter.string)
        var str = string.replace("\\", "\\\\")
        str = str.replace("\"", "\\\"")

        this.writer.write(str)
        this.writer.write(Json.StringDelimiter.string)
    }
}
