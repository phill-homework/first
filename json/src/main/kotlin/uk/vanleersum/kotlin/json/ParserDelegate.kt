package uk.vanleersum.kotlin.json

/**
 * @author phill Interface for responding to Element Parsing
 */
public interface ParserDelegate {
    /**
     * The parser is completed parsing a list with a given name.
     *
     * @param parser
     * @param key
     * string the list is stored against if the list is stored in a map,
     * null otherwise (for example in a list)
     */
    public fun jsonDidParseList(parser: Parser, key: String?) {}

    /**
     * The parser is completed parsing a map with a given name.
     *
     * @param parser
     * @param key
     * string the map is stored against if the map is stored in a map,
     * null otherwise (for example in a list)
     */
    public fun jsonDidParseMap(parser: Parser, key: String?) {}

    /**
     * @param parser
     * @param key
     * string the string is stored against if the string is stored in a
     * map, null otherwise (for example in a list)
     * @param value
     * the string parsed
     */
    public fun jsonDidParseString(parser: Parser, key: String?, value: String) {}

    /**
     * The parser is about to parse a list with a given name.
     *
     * @param parser
     * @param key
     *
     * string the list is stored against if the list is stored in a map,
     * null otherwise (for example in a list)
     */
    public fun jsonWillParseList(parser: Parser, key: String?) {}

    /**
     * The parser is about to parse a map with a given name.
     * @param parser
     * @param key
     * string the map is stored against if the map is stored in a map,
     * null otherwise (for example in a list)
     */
    public fun jsonWillParseMap(parser: Parser, key: String?) {}

    /**
     * A delegate was popped from the delegate stack.  This is handy for the 'parent' delegate
     * to process the data gathered by the child delegate.
     * @param parser
     */
    public fun jsonDidPopDelegate(parser: Parser, delegate: ParserDelegate) {}
}