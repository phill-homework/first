package uk.vanleersum.kotlin.json

public class UnparserException(message: String) : java.lang.Exception(message)