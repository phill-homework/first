package com.singerinstruments.tcats.jfx

import com.singerinstruments.tcats.model.TechUnitType
import javafx.scene.control.Label
import javafx.scene.control.ListCell
import javafx.scene.control.Tooltip

/**
 * Represents an instance of TechUnitType in a ListView
 */
class UnitTypeListCell : ListCell<TechUnitType?>() {
    private val label = Label()
    override fun updateItem(item: TechUnitType?, empty: Boolean) {
        super.updateItem(item, empty)
        this.label.text = ""
        this.label.tooltip = null
        if (empty) return
        item ?: return
        this.label.tooltip = Tooltip("uid: ${item.uid}")
        this.label.text = item.name
        this.graphic = label
    }
}