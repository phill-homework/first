package com.singerinstruments.tcats.jfx

import com.singerinstruments.tcats.viewmodel.Preferences
import com.singerinstruments.tcats.viewmodel.ViewModel
import javafx.event.EventHandler
import javafx.scene.control.Menu
import javafx.scene.control.MenuBar
import javafx.scene.control.MenuItem
import javafx.stage.FileChooser
import java.io.File

class MenuBar(private val viewModel: ViewModel) : MenuBar() {
    private val fileExtension = "tcats"
    private val fileExtensionWithDot = "." + this.fileExtension

    val currentFileName get() = Preferences.Shared.currentFileName()

    init {
        this.menus.add(Menu("File").apply {
            items.addAll(
                MenuItem("File").apply {
                    onAction = EventHandler { this@MenuBar.newTcatsFile() }
                },
                MenuItem("Open...").apply {
                    onAction = EventHandler { this@MenuBar.openTcatsFile() }
                },
                MenuItem("Save").apply {
                    onAction = EventHandler { this@MenuBar.saveTcatsFile() }
                },
                MenuItem("Save As...").apply {
                    onAction = EventHandler { this@MenuBar.saveAsTcatsFile() }
                },
                MenuItem("Exit").apply {
                    onAction = EventHandler { System.exit(0) }
                }
            )
        })
//        this.menus.add(Menu("Data").apply {
//            items.addAll(
//                    MenuItem("Refresh Actions").apply {
//                        onAction = EventHandler { this@MenuBar.viewModel.refreshScienceActions() }
//                    },
//                    MenuItem("Add Action").apply {
//                        onAction = EventHandler { this@MenuBar.addScienceAction() }
//                    },
//            )
//        })
    }

//    private fun addScienceAction() {
//        val dialog = TextInputDialog("")
//        dialog.title = "Add new action"
//        dialog.headerText = null
//        dialog.contentText = "Action name:"
//        dialog.showAndWait().ifPresent { str -> this.viewModel.addScienceAction(str) }
//    }

    private fun newTcatsFile() {
        //this.viewModel.newTcats()
        Preferences.Shared.currentFileName("")
    }

    private fun openTcatsFile() {
        val fileChooser = FileChooser()
        fileChooser.initialDirectory = currentDirectory()
        fileChooser.title = "Open Tcats File"
        fileChooser.extensionFilters.addAll(
            FileChooser.ExtensionFilter("Tcats Files", "*" + this.fileExtensionWithDot)
        )
//        val selectedFile = fileChooser.showOpenDialog(null)
//        if (selectedFile != null) {
//            if (this.viewModel.loadTcats(selectedFile)) {
//                Preferences.Shared.currentFileName(selectedFile.absolutePath)
//            }
//        }
    }

    private fun saveTcatsFile() {
        val filename = this.currentFileName
        if (filename.isNullOrEmpty()) {
            this.saveAsTcatsFile()
//        } else {
//            val file = File(filename)
//            if (this.viewModel.saveTcats(file)) {
//                Preferences.Shared.currentFileName(file.absolutePath)
//            }
        }
    }

    private fun saveAsTcatsFile() {
        val fileChooser = FileChooser()
        fileChooser.initialDirectory = currentDirectory()
        fileChooser.title = "Save Tcats File"
        fileChooser.extensionFilters.addAll(
            FileChooser.ExtensionFilter("TcatsFiles Files", "*" + this.fileExtensionWithDot)
        )
//        val file = fileChooser.showSaveDialog(null)
//        if (null != file) {
//            if (file.absoluteFile.path.toLowerCase().endsWith(fileExtensionWithDot)) {
//                this.viewModel.saveTcats(file)
//            } else {
//                this.viewModel.saveTcats(File(file.absolutePath + fileExtensionWithDot))
//            }
//        }
    }

    private fun currentDirectory(): File {
        var currentDirectoryName = System.getProperty("user.home")
        val currentFileName = this.currentFileName
        if (null != currentFileName && !currentFileName.isEmpty()) {
            var f = File(currentFileName)
            f = f.parentFile
            currentDirectoryName = f.absolutePath
            try {
                currentDirectoryName = f.canonicalPath
            } catch (e: Exception) {
            }
        }
        return File(currentDirectoryName)
    }
}