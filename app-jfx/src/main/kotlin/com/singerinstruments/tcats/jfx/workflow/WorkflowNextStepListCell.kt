package com.singerinstruments.tcats.jfx.workflow

import com.singerinstruments.tcats.model.WorkflowNextStep
import javafx.scene.control.Label
import javafx.scene.control.ListCell

/**
 * Represents an instance of WorkflowNextStep in a ListView
 */
class WorkflowNextStepListCell : ListCell<WorkflowNextStep?>() {
    private val label = Label()
    override fun updateItem(item: WorkflowNextStep?, empty: Boolean) {
        super.updateItem(item, empty)
        this.label.text = ""
        if (empty) return
        item ?: return
        this.label.text = item.nextStep?.id?.toString() ?: "(Step ${item.nextStepId} not found"
        this.graphic = label
    }
}