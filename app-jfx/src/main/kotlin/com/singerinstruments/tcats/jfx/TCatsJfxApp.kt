package com.singerinstruments.tcats.jfx

import javafx.application.Application
import javafx.scene.Scene
import javafx.stage.Stage

class TCatsJfxApp : Application() {
    private val mainView = MainView(this)
    lateinit var stage: Stage
    private val titleBase = "TCats Kotlin JavaFx App: "

    override fun start(primaryStage: Stage?) {
        primaryStage?.apply {
            stage = this
            scene = Scene(this@TCatsJfxApp.mainView)
            title = titleBase
            height = 375.0
            width = 667.0
            show()
        }
    }

    var title: String
        get() = stage.title
        set(value) {
            stage.title = value
        }

    fun composeTitle(source: String) {

        this.title = titleBase + source
    }

    override fun stop() {
        this.mainView.willStop()
        super.stop()
    }

    companion object {
        // The easiest way to run this is using gradle task 'application/run'
        // Otherwise you will need to point IDE at JavaFx library.....
        @JvmStatic
        fun main(args: Array<String>) {
            launch(TCatsJfxApp::class.java)
        }
    }
}

