package com.singerinstruments.tcats.jfx

import com.singerinstruments.kotlin.common.TitlePane
import com.singerinstruments.tcats.data.UnitTypeDataList
import com.singerinstruments.tcats.model.Action
import com.singerinstruments.tcats.model.TechUnitType
import com.singerinstruments.tcats.viewmodel.UnitTypesViewModel
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.scene.control.Button
import javafx.scene.control.ListCell
import javafx.scene.control.ListView
import javafx.scene.control.Tab
import javafx.scene.layout.BorderPane
import javafx.scene.layout.FlowPane
import javafx.scene.layout.GridPane
import javafx.util.Callback

class UnitTypeTab(private val viewModel: UnitTypesViewModel) : Tab(UnitTypeDataList.logicalPath) {

    init {
        // basic layout
        val gridPane = GridPane()
        val buttons = FlowPane(
            Button("Refresh Types").apply { onAction = EventHandler { viewModel.refresh() } },
            Button("Add Type").apply { onAction = EventHandler { addModuleType() } }
        ).apply {
            hgap = 10.0
            padding = Insets(5.0)
        }
        this.content = BorderPane(gridPane, null, null, TitlePane("", buttons), null)

        val scienceActionsList = ListView<Action>()


        val moduleTypeList = ListView<TechUnitType>().apply {
            cellFactory = object : Callback<ListView<TechUnitType?>?, ListCell<TechUnitType?>?> {
                override fun call(list: ListView<TechUnitType?>?) = UnitTypeListCell()
            }
            selectionModel.selectedItemProperty().addListener { _, _, moduleType ->
                scienceActionsList.items.setAll(moduleType?.actions ?: emptyList())
            }
        }

        // add the list views to the gridPane
        gridPane.add(TitlePane("Module types", moduleTypeList), 0, 0)
        gridPane.add(TitlePane("Science actions", scienceActionsList), 1, 0)

        this.viewModel.add(object : UnitTypesViewModel.Listener {
            override fun changed(viewModel: UnitTypesViewModel) {
                moduleTypeList.items.setAll(viewModel.techUnitTypes)
            }
        })
    }

    private fun addModuleType() {
        val dialog = CreateUnitTypeDialog(viewModel.model.actionSource)
//        dialog.title = "Add new action"
//        dialog.headerText = null
//        dialog.contentText = "Action name:"
        dialog.showAndWait().ifPresent {
            println(it.json)
            this.viewModel.createModuleType(it)
        }
    }
}