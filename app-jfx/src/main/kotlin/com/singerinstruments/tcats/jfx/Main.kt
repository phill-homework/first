package com.singerinstruments.tcats.jfx

//https://stackoverflow.com/questions/52569724/javafx-11-create-a-jar-file-with-gradle
object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        TCatsJfxApp.main(args)
    }
}