package com.singerinstruments.tcats.jfx

import com.singerinstruments.kotlin.common.TitlePane
import com.singerinstruments.tcats.data.WallData
import com.singerinstruments.tcats.data.WallDataList
import com.singerinstruments.tcats.jfx.walls.*
import com.singerinstruments.tcats.jfx.walls.dialog.EditWallDialogue
import com.singerinstruments.tcats.model.Action
import com.singerinstruments.tcats.model.TechUnit
import com.singerinstruments.tcats.model.Wall
import com.singerinstruments.tcats.viewmodel.WallsViewModel
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.scene.control.*
import javafx.scene.layout.*
import javafx.util.Callback
import java.time.LocalDateTime

class WallsTab(private val viewModel: WallsViewModel) : Tab(WallDataList.logicalPath) {
    init {

        val refreshButton = Button("Refresh Walls").apply {
            onAction = EventHandler { this@WallsTab.viewModel.refresh() }
        }
        val addButton = Button("Add Wall").apply {
            onAction = EventHandler { this@WallsTab.addInstallation() }
        }
        val editButton = Button("Edit Wall").apply {
            isDisable = true
        }

        val buttons = FlowPane(refreshButton, addButton, editButton).apply {
            hgap = 10.0
            padding = Insets(5.0)
        }

        val gridPane = GridPane()

        val column0 = ColumnConstraints()
        val column1 = ColumnConstraints()
        val column2 = ColumnConstraints()
        val column3 = ColumnConstraints()
        column3.hgrow = Priority.ALWAYS

        gridPane.columnConstraints.addAll(column0, column1, column2, column3)


        val detailsView = UnitDetailView()

        val moduleList = ListView<TechUnit>().apply {
            cellFactory = object : Callback<ListView<TechUnit?>?, ListCell<TechUnit?>?> {
                override fun call(list: ListView<TechUnit?>?) = UnitListCell()
            }
            // TODO: should we notify ViewModel, and update upon viewModel change?
            selectionModel.selectedItemProperty().addListener { _, _, module ->
                detailsView.module = module
            }
        }

        val availableActionsList = ListView<Action>().apply {
            cellFactory = object : Callback<ListView<Action?>?, ListCell<Action?>?> {
                override fun call(list: ListView<Action?>?) = ActionListCell()
            }
        }

        val installationsList = ListView<Wall>().apply {
            cellFactory = object : Callback<ListView<Wall?>?, ListCell<Wall?>?> {
                override fun call(list: ListView<Wall?>?) = WallListCell()
            }
            items.setAll(viewModel.installations)
            // TODO: should we notify ViewModel, and update upon viewModel change?
            selectionModel.selectedItemProperty().addListener { _, _, install ->
                moduleList.items.setAll(install?.units ?: emptyList())
                availableActionsList.items.setAll(install?.availableActions ?: emptyList())
                editButton.isDisable = null == install
            }
        }

        gridPane.add(TitlePane("installations", installationsList), 0, 0, 1, 2)
        gridPane.add(TitlePane("modules", moduleList), 1, 0)
        gridPane.add(TitlePane("available actions", availableActionsList), 1, 1)
        gridPane.add(TitlePane("Module Details", detailsView), 2, 0, 1, 2)




        editButton.onAction = EventHandler {
            installationsList.selectionModel.selectedItem?.also {
                this.editInstallation(it)
            }
        }

        this.content = BorderPane(gridPane, null, null, TitlePane("", buttons), null)

        this.viewModel.add(object : WallsViewModel.Listener {
            override fun changed(viewModel: WallsViewModel) {
                installationsList.items.setAll(viewModel.installations)
            }
        })

    }

    private fun addInstallation() {
        val dlg = EditWallDialogue(this.viewModel.unitTypeSource, this.viewModel.createInstallation())
        dlg.showAndWait().ifPresent { moduleData ->
            val installationData = WallData(LocalDateTime.now().toString(), moduleData)
            this.viewModel.putInstallationData(installationData)
        }
    }

    private fun editInstallation(wall: Wall) {
        val dlg = EditWallDialogue(this.viewModel.unitTypeSource, wall)
        dlg.showAndWait().ifPresent { moduleData ->
            val wallData = WallData(wall.name, moduleData, wall.uid)
            this.viewModel.putInstallationData(wallData)
        }
    }
}


