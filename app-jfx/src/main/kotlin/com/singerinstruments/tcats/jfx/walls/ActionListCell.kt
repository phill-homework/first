package com.singerinstruments.tcats.jfx.walls

import com.singerinstruments.tcats.model.Action
import javafx.scene.control.Label
import javafx.scene.control.ListCell
import javafx.scene.control.Tooltip

class ActionListCell : ListCell<Action?>() {
    private val label = Label()
    override fun updateItem(item: Action?, empty: Boolean) {
        super.updateItem(item, empty)
        this.label.text = ""
        this.label.tooltip = null
        if (empty) return
        item ?: return
        this.label.tooltip = Tooltip("uid: ${item.uid}")
        this.label.text = item.name
        this.graphic = label
    }
}