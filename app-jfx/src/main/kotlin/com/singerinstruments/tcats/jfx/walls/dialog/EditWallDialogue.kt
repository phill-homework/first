package com.singerinstruments.tcats.jfx.walls.dialog

import com.singerinstruments.tcats.data.UnitData
import com.singerinstruments.tcats.model.UnitTypeSource
import com.singerinstruments.tcats.model.Wall
import com.singerinstruments.techwall.shared.ModuleGrid
import javafx.scene.control.ButtonType
import javafx.scene.control.Dialog
import javafx.scene.control.ScrollPane

class EditWallDialogue(moduleTypeSource: UnitTypeSource, wall: Wall) : Dialog<List<UnitData>>() {
    init {
        this.dialogPane.buttonTypes.addAll(ButtonType.OK, ButtonType.CANCEL)

        val moduleGrid = ModuleGrid(wall.units, moduleTypeSource)
        this.dialogPane.content = ScrollPane(moduleGrid)
        this.isResizable = true
        setResultConverter { dialogButton ->
            if (ButtonType.OK == dialogButton) {
                moduleGrid.moduleData
            } else {
                null
            }
        }
    }
}