package com.singerinstruments.tcats.jfx

import com.singerinstruments.tcats.data.DataModel
import com.singerinstruments.tcats.dataservices.BaseAddress
import com.singerinstruments.tcats.dataservices.DataService
import com.singerinstruments.tcats.dataservices.ephemeral.EphemeralDataService
import com.singerinstruments.tcats.dataservices.http.HttpDataService
import com.singerinstruments.tcats.jfx.workflow.WorkflowsTab
import com.singerinstruments.tcats.model.Model
import com.singerinstruments.tcats.viewmodel.Preferences
import com.singerinstruments.tcats.viewmodel.ViewModel
import javafx.event.EventHandler
import javafx.scene.control.Button
import javafx.scene.control.TabPane
import javafx.scene.layout.BorderPane
import javafx.scene.layout.FlowPane
import javafx.stage.FileChooser
import java.io.File


class MainView(val app: TCatsJfxApp) : BorderPane() {

    private var dataService: DataService = EphemeralDataService()
    private val viewModel = ViewModel(Model(this.dataService))

    init {

        this.top = FlowPane(
            Button("New...").apply { onAction = EventHandler { newModel() } },
            Button("Connect...").apply { onAction = EventHandler { connect() } },
            Button("Load...").apply { onAction = EventHandler { load() } },
            Button("Save...").apply { onAction = EventHandler { save() } },
            Button("Exit").apply { onAction = EventHandler { System.exit(0) } },
        )

        this.center = TabPane(
            ActionsTab(this.viewModel.actionsViewModel),
            UnitTypeTab(this.viewModel.unitTypesViewModel),
            WallsTab(this.viewModel.wallsViewModel),
            WorkflowsTab(this.viewModel.workflowsViewModel)
        )
    }

    fun willStop() {
        this.dataService.stop()
    }

    private fun title(string: String) {
        app.composeTitle(string)
    }

    private fun connect() {
        ConnectDialogue().showAndWait().ifPresent { (path, port) ->
            this.dataService = HttpDataService(BaseAddress(path, port))
            this.viewModel.model = Model(this.dataService)
            Preferences.Shared.currentFileName("")
            title("${path}:${port}")
        }
    }

    private fun newModel() {
        this.dataService = EphemeralDataService()
        this.viewModel.model = Model(dataService)
        Preferences.Shared.currentFileName("")
        title("")
    }

    private fun load() {
        val fileChooser = FileChooser()
        if (currentDirectory().isDirectory) {
            fileChooser.initialDirectory = currentDirectory()
        }
        fileChooser.title = "Open TCats Model File"
        fileChooser.extensionFilters.addAll(
            FileChooser.ExtensionFilter("TCat Model Files", "*" + ViewModel.fileExtensionWithDot),
            FileChooser.ExtensionFilter("All Files", "*")
        )
        fileChooser.showOpenDialog(null)?.also {
            try {
                val modelData = DataModel.from(it)
                this.dataService = EphemeralDataService(modelData)
                this.viewModel.model = Model(dataService)
                Preferences.Shared.currentFileName(it.absolutePath)
                title(Preferences.Shared.currentFileName() ?: "")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun save() {
        val fileChooser = FileChooser()
        fileChooser.initialDirectory = currentDirectory()
        fileChooser.title = "Save TCats Model File"
        fileChooser.extensionFilters.addAll(
            FileChooser.ExtensionFilter("TCat Model Files", "*" + ViewModel.fileExtensionWithDot)
        )
        fileChooser.showSaveDialog(null).also {
            try {
                DataModel.write(this.viewModel.model.data, it)
                Preferences.Shared.currentFileName(it.absolutePath)
                title(Preferences.Shared.currentFileName() ?: "")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun currentDirectory(): File {
        var currentDirectoryName = System.getProperty("user.home")
        val currentFileName = Preferences.Shared.currentFileName()
        if (null != currentFileName && !currentFileName.isEmpty()) {
            var f = File(currentFileName)
            f = f.parentFile
            currentDirectoryName = f.absolutePath
            try {
                currentDirectoryName = f.canonicalPath
            } catch (e: Exception) {
            }
        }
        return File(currentDirectoryName)
    }
}

