package com.singerinstruments.tcats.jfx

import javafx.application.Platform
import javafx.geometry.Insets
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.layout.GridPane

internal class ConnectDialogue : Dialog<Pair<String, Int>?>() {
    init {
        this.title = "Connect Dialog"
        this.headerText = "Select destination details"
        //this.graphic = ImageView(this.javaClass.getResource("login.png").toString())

        val connectButtonType = ButtonType("Connect", ButtonBar.ButtonData.OK_DONE)
        this.dialogPane.buttonTypes.addAll(connectButtonType, ButtonType.CANCEL)

        val grid = GridPane()
        grid.hgap = 10.0
        grid.vgap = 10.0
        grid.padding = Insets(20.0, 150.0, 10.0, 10.0)

        val addressTextfield = TextField().apply {
            promptText = "Address"
            text = "localhost"
        }
        val portTextfield = TextField().apply {
            promptText = "Port"
            text = "8081"
            textProperty().addListener { _, _, newValue ->
                if (!newValue.matches("\\d*".toRegex())) {
                    text = newValue.replace("[^\\d]".toRegex(), "")
                }
            }
        }

        val protocol = "http://"
        grid.add(Label("Address:"), 0, 0)
        grid.add(Label(protocol), 1, 0)
        grid.add(addressTextfield, 2, 0)
        grid.add(Label("Port:"), 0, 1)
        grid.add(portTextfield, 1, 1, 2, 1)

        val loginButton: Node = this.dialogPane.lookupButton(connectButtonType)

        addressTextfield.textProperty().addListener { _, _, _ ->
            loginButton.isDisable = addressTextfield.text.isBlank() || portTextfield.text.isBlank()
        }
        portTextfield.textProperty().addListener { _, _, _ ->
            loginButton.isDisable = addressTextfield.text.isBlank() || portTextfield.text.isBlank()
        }

        this.dialogPane.content = grid

        Platform.runLater { addressTextfield.requestFocus() }

        this.setResultConverter { dialogButton: ButtonType ->
            if (dialogButton == connectButtonType) {
                return@setResultConverter Pair(protocol + addressTextfield.text, portTextfield.text.toInt())
            }
            null
        }
    }
}