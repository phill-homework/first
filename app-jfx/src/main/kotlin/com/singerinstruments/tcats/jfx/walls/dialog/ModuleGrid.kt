package com.singerinstruments.techwall.shared

import com.singerinstruments.tcats.data.UnitData
import com.singerinstruments.tcats.model.TechUnit
import com.singerinstruments.tcats.model.UnitTypeSource
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.scene.control.ContextMenu
import javafx.scene.control.MenuItem
import javafx.scene.layout.GridPane

class ModuleGrid(techUnits: List<TechUnit>, val moduleTypeSource: UnitTypeSource) : GridPane() {

    private var moduleViews = techUnits.map { UnitView(it) }.toMutableList()
    private var addedCount = 0
    val moduleData: List<UnitData> get() = this.moduleViews.map { it.moduleData }

    private fun logicalRow(row: Int) = (moduleViews.maxOfOrNull { it.row } ?: 0) - row

    init {
        hgap = 10.0
        vgap = 10.0
        padding = Insets(10.0)
        draw()
    }

    private fun draw() {
        this.moduleViews.forEach { moduleView ->
            this.children.remove(moduleView)
        }
        //println()
        this.moduleViews.forEach { moduleView ->
            //println("${moduleView.techUnitType.name} : ${moduleView.row}, ${moduleView.column}")
            this.add(moduleView, moduleView.column, this.logicalRow(moduleView.row))
            moduleView.onMouseClicked = EventHandler {
                if (it.clickCount > 1) {
                    this@ModuleGrid.edit(moduleView)
                }
            }
            moduleView.setOnContextMenuRequested {
                val menu = ContextMenu()
                menu.items.apply {
                    add(MenuItem("Edit").also { it.onAction = EventHandler { this@ModuleGrid.edit(moduleView) } })
                    add(MenuItem("Delete").also { it.onAction = EventHandler { this@ModuleGrid.delete(moduleView) } })
                    add(MenuItem("Add left").also {
                        it.onAction = EventHandler { this@ModuleGrid.addLeft(moduleView) }
                    })
                    add(MenuItem("Add right").also {
                        it.onAction = EventHandler { this@ModuleGrid.addRight(moduleView) }
                    })
                    add(MenuItem("Add above").also {
                        it.onAction = EventHandler { this@ModuleGrid.addAbove(moduleView) }
                    })
                    add(MenuItem("Add below").also {
                        it.onAction = EventHandler { this@ModuleGrid.addBelow(moduleView) }
                    })
                }
                menu.show(this, it.screenX, it.screenY)
            }
        }
    }

    private fun occupied(row: Int, column: Int) = this.moduleViews.any { row == it.row && column == it.column }

    private fun moveRight(from: Int) = this.moduleViews
        .filter { it.column >= from }
        .forEach { it.column++ }

    private fun moveUp(from: Int) = this.moduleViews
        .filter { it.row >= from }
        .forEach { it.row++ }

    private fun edit(src: UnitView) = this.moduleTypeSource.also { EditUnitDialogue(it, src).showAndWait() }

    private fun addLeft(src: UnitView) {
        var addColumn = src.column - 1
        if (0 == src.column) {
            addColumn = src.column
            this.moveRight(0)
        } else if (this.occupied(src.row, src.column - 1)) {
            addColumn = src.column
            this.moveRight(src.column)
        }
        this.moduleViews.add(UnitView(src.row, addColumn, "added${++this.addedCount}", src.techUnitType, ""))
        draw()
    }

    private fun addRight(src: UnitView) {
        val addColumn = src.column + 1
        if (this.occupied(src.row, addColumn)) {
            this.moveRight(addColumn)
        }
        this.moduleViews.add(UnitView(src.row, addColumn, "added${++this.addedCount}", src.techUnitType, ""))
        draw()
    }

    private fun addAbove(src: UnitView) {
        val addRow = src.row + 1
        if (this.occupied(addRow, src.column)) {
            this.moveUp(addRow)
        }
        this.moduleViews.add(UnitView(addRow, src.column, "added${++this.addedCount}", src.techUnitType, ""))
        draw()
    }

    private fun addBelow(src: UnitView) {
        var addRow = src.row - 1
        if (0 == src.row) {
            addRow = src.row
            this.moveUp(0)
        } else if (this.occupied(addRow, src.column)) {
            addRow = src.row
            this.moveUp(addRow)
        }
        this.moduleViews.add(UnitView(addRow, src.column, "added${++this.addedCount}", src.techUnitType, ""))
        draw()
    }

    private fun delete(src: UnitView) {
        if (this.moduleViews.size < 2) {
            return
        }
        this.moduleViews.remove(src)
        if (this.moduleViews.none { it.column == src.column }) {
            // no other unit in column so delete column
            this.moduleViews
                .filter { it.column > src.column }
                .forEach { it.column-- }
        }
        if (this.moduleViews.none { it.row == src.row }) {
            //no other unit in row so delete row
            this.moduleViews
                .filter { it.row > src.row }
                .forEach { it.row-- }
        }
        draw()
    }
}