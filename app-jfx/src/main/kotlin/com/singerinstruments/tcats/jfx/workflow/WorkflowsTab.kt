package com.singerinstruments.tcats.jfx.workflow

import com.singerinstruments.kotlin.common.TitlePane
import com.singerinstruments.tcats.data.WorkflowDataList
import com.singerinstruments.tcats.model.Workflow
import com.singerinstruments.tcats.model.WorkflowNextStep
import com.singerinstruments.tcats.model.WorkflowStep
import com.singerinstruments.tcats.model.WorkflowVersion
import com.singerinstruments.tcats.viewmodel.WorkflowsViewModel
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.scene.control.Button
import javafx.scene.control.ListCell
import javafx.scene.control.ListView
import javafx.scene.control.Tab
import javafx.scene.layout.BorderPane
import javafx.scene.layout.FlowPane
import javafx.scene.layout.GridPane
import javafx.util.Callback


class WorkflowsTab(private val viewModel: WorkflowsViewModel) : Tab(WorkflowDataList.logicalPath) {

    init {
        // basic layout
        val gridPane = GridPane()
        val buttons = FlowPane(
            Button("Refresh Workflows").apply { onAction = EventHandler { viewModel.refresh() } },
            Button("Add Workflow").apply {
                isDisable = true
                onAction = EventHandler { addWorkflow() }
            }
        ).apply {
            hgap = 10.0
            padding = Insets(5.0)
        }
        this.content = BorderPane(gridPane, null, null, TitlePane("", buttons), null)

        // build the list views
        val nextStepsList = ListView<WorkflowNextStep>().apply {
            cellFactory = object : Callback<ListView<WorkflowNextStep?>?, ListCell<WorkflowNextStep?>?> {
                override fun call(list: ListView<WorkflowNextStep?>?) = WorkflowNextStepListCell()
            }
        }

        val stepsList = ListView<WorkflowStep>().apply {
            cellFactory = object : Callback<ListView<WorkflowStep?>?, ListCell<WorkflowStep?>?> {
                override fun call(list: ListView<WorkflowStep?>?) = WorkflowStepListCell()
            }
            // TODO: should we notify ViewModel, and update upon viewModel change?
            selectionModel.selectedItemProperty().addListener { _, _, step ->
                nextStepsList.items.setAll(step?.nextSteps ?: emptyList())
            }
        }

        val versionsList = ListView<WorkflowVersion>().apply {
            cellFactory = object : Callback<ListView<WorkflowVersion?>?, ListCell<WorkflowVersion?>?> {
                override fun call(list: ListView<WorkflowVersion?>?) = WorkflowVersionListCell()
            }
            // TODO: should we notify ViewModel, and update upon viewModel change?
            selectionModel.selectedItemProperty().addListener { _, _, version ->
                stepsList.items.setAll(version?.steps ?: emptyList())
            }
        }

        val workflowsList = ListView<Workflow>().apply {
            cellFactory = object : Callback<ListView<Workflow?>?, ListCell<Workflow?>?> {
                override fun call(list: ListView<Workflow?>?) = WorkflowListCell()
            }
            // TODO: should we notify ViewModel, and update upon viewModel change?
            selectionModel.selectedItemProperty().addListener { _, _, workflow ->
                versionsList.items.setAll(workflow?.versions ?: emptyList())
            }
        }

        // add the list views to the gridPane
        gridPane.add(TitlePane("Workflows", workflowsList), 0, 0)
        gridPane.add(TitlePane("Versions", versionsList), 1, 0)
        gridPane.add(TitlePane("Steps", stepsList), 2, 0)
        gridPane.add(TitlePane("Next steps", nextStepsList), 3, 0)

        // listen to the viewModel workflows changing
        this.viewModel.add(object : WorkflowsViewModel.Listener {
            override fun changed(viewModel: WorkflowsViewModel) {
                workflowsList.items.setAll(viewModel.workflows)
            }
        })
    }

    private fun addWorkflow() {
//        val dialog = TextInputDialog("")
//        dialog.title = "Add new action"
//        dialog.headerText = null
//        dialog.contentText = "Action name:"
//        dialog.showAndWait().ifPresent { str -> this.viewModel.addScienceAction(str) }
    }
}

