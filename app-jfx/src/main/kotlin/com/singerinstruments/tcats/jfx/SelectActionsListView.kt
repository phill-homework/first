package com.singerinstruments.tcats.jfx

import com.singerinstruments.tcats.model.Action
import com.singerinstruments.tcats.model.ActionSource
import javafx.beans.property.SimpleBooleanProperty
import javafx.scene.control.ListCell
import javafx.scene.control.ListView
import javafx.scene.control.Tooltip
import javafx.scene.control.cell.CheckBoxListCell
import javafx.util.Callback
import javafx.util.StringConverter

class SelectActionsListView(scienceActionSource: ActionSource) : ListView<Action>() {
    val map = scienceActionSource
        .map { it to SimpleBooleanProperty() }
        .toMap()

    init {
        cellFactory = Callback<ListView<Action?>?, ListCell<Action?>?> {
            val sc = object : StringConverter<Action>() {
                override fun toString(action: Action?) = action?.name ?: ""

                override fun fromString(string: String?): Action {
                    TODO("Not yet implemented")
                }
            }

            object : CheckBoxListCell<Action>({ a -> map[a] }, sc) {
                override fun updateItem(item: Action?, empty: Boolean) {
                    super.updateItem(item, empty)
                    this.tooltip = Tooltip(item?.uid ?: "")
                }
            }
        }

        this.items.setAll(scienceActionSource.toList())
    }

    val included get() = this.map.filter { it.value.get() }.map { it.key.uid }
}