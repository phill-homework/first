package com.singerinstruments.tcats.jfx

import com.singerinstruments.kotlin.common.TitlePane
import com.singerinstruments.tcats.data.ActionDataList
import com.singerinstruments.tcats.model.Action
import com.singerinstruments.tcats.viewmodel.ActionsViewModel
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.scene.control.*
import javafx.scene.layout.BorderPane
import javafx.scene.layout.FlowPane

class ActionsTab(private val viewModel: ActionsViewModel) : Tab(ActionDataList.logicalPath) {
    private val scienceActionsList = ListView<Action>()

    init {
        scienceActionsList.items.setAll(viewModel.actions)

        val refreshButton = Button("Refresh Actions").apply {
            onAction = EventHandler { this@ActionsTab.viewModel.refresh() }
        }
        val addButton = Button("Add Action").apply {
            onAction = EventHandler { this@ActionsTab.addScienceAction() }
        }
        val buttons = FlowPane(refreshButton, addButton).apply {
            hgap = 10.0
            padding = Insets(5.0)
        }

        this.content = BorderPane(scienceActionsList, null, null, TitlePane("", buttons), null)

        this.viewModel.add(object : ActionsViewModel.Listener {
            override fun changed(viewModel: ActionsViewModel) {
                scienceActionsList.items.setAll(viewModel.actions)
            }
        })
    }

    private fun addScienceAction() {
        val dialog = TextInputDialog("")
        dialog.title = "Add new action"
        dialog.headerText = null
        dialog.contentText = "Action name:"
        dialog.showAndWait().ifPresent { str -> this.viewModel.addScienceAction(str) }
    }
}


