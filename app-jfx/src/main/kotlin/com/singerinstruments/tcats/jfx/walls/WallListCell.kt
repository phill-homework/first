package com.singerinstruments.tcats.jfx.walls

import com.singerinstruments.tcats.model.Wall
import javafx.scene.control.Label
import javafx.scene.control.ListCell
import javafx.scene.control.Tooltip

/**
 * Represents an instance of Wall in a ListView
 */
class WallListCell : ListCell<Wall?>() {
    private val label = Label()
    override fun updateItem(item: Wall?, empty: Boolean) {
        super.updateItem(item, empty)
        this.label.tooltip = null
        this.label.text = ""
        if (empty) return
        item ?: return
        this.label.tooltip = Tooltip("uid: ${item.uid}")
        this.label.text = item.name
        this.graphic = label
    }
}