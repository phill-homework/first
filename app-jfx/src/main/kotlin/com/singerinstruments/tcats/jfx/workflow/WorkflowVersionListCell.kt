package com.singerinstruments.tcats.jfx.workflow

import com.singerinstruments.tcats.model.WorkflowVersion
import javafx.scene.control.Label
import javafx.scene.control.ListCell

/**
 * Represents an instance of WorkflowVersion in a ListView
 */
class WorkflowVersionListCell : ListCell<WorkflowVersion?>() {
    private val label = Label()
    override fun updateItem(item: WorkflowVersion?, empty: Boolean) {
        super.updateItem(item, empty)
        this.label.text = ""
        if (empty) return
        item ?: return
        this.label.text = item.version.toString()
        this.graphic = label
    }
}