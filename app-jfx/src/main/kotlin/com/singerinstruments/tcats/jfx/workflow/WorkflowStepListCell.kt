package com.singerinstruments.tcats.jfx.workflow

import com.singerinstruments.tcats.model.WorkflowStep
import javafx.scene.control.Label
import javafx.scene.control.ListCell

/**
 * Represents an instance of WorkflowStep in a ListView
 */
class WorkflowStepListCell : ListCell<WorkflowStep?>() {
    private val label = Label()
    override fun updateItem(item: WorkflowStep?, empty: Boolean) {
        super.updateItem(item, empty)
        this.label.text = ""
        if (empty) return
        item ?: return
        val msg = item.action?.name ?: "(action not found: ${item.actionId})"
        this.label.text = "${item.id}: $msg"
        this.graphic = label
    }
}