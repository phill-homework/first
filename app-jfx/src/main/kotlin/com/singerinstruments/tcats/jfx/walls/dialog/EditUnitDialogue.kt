package com.singerinstruments.techwall.shared

import com.singerinstruments.tcats.model.TechUnitType
import com.singerinstruments.tcats.model.UnitTypeSource
import javafx.geometry.Insets
import javafx.scene.control.*
import javafx.scene.layout.GridPane

class EditUnitDialogue(moduleTypeSource: UnitTypeSource, val unit: UnitView) : Dialog<Boolean>() {
    init {
        this.dialogPane.buttonTypes.addAll(ButtonType.OK, ButtonType.CANCEL)
        val grid = GridPane()
        grid.hgap = 10.0
        grid.vgap = 10.0
        grid.padding = Insets(20.0, 150.0, 10.0, 10.0)

        val moduleName = TextField(unit.name)
        moduleName.promptText = "unit name"

        val moduleType = ComboBox<TechUnitType>().apply {
            items.setAll(moduleTypeSource.toList())
            selectionModel.select(unit.techUnitType)
        }

        grid.add(Label("Module name:"), 0, 0)
        grid.add(moduleName, 1, 0)
        grid.add(Label("Module techUnitType:"), 0, 1)
        grid.add(moduleType, 1, 1)
        this.dialogPane.content = grid

        setResultConverter { dialogButton ->
            if (ButtonType.OK == dialogButton) {
                unit.name = moduleName.text
                unit.techUnitType = moduleType.selectionModel.selectedItem
            }
            ButtonType.OK == dialogButton
        }
    }
}