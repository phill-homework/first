package com.singerinstruments.tcats.jfx.walls

import com.singerinstruments.tcats.model.Action
import com.singerinstruments.tcats.model.TechUnit
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.control.ListCell
import javafx.scene.control.ListView
import javafx.scene.control.TitledPane
import javafx.scene.layout.GridPane
import javafx.util.Callback

class UnitDetailView : GridPane() {

    internal var module: TechUnit? = null
        set(value) {
            field = value
            this.setFields()
        }

    private val moduleTypeLabel = Label()
    private val locationLabel = Label()
    private val scienceActionsList = ListView<Action>().apply {
        cellFactory = object : Callback<ListView<Action?>?, ListCell<Action?>?> {
            override fun call(list: ListView<Action?>?) = ActionListCell()
        }
    }

    init {
        hgap = 4.0
        add(Label("Module techUnitType:").apply { alignment = Pos.BASELINE_RIGHT }, 0, 0)
        add(this.moduleTypeLabel, 1, 0)

        add(Label("Location:").apply { alignment = Pos.BASELINE_RIGHT }, 0, 1)
        add(this.locationLabel, 1, 1)

        add(TitledPane("Actions:", this.scienceActionsList), 0, 2, 2, 1)
        //add(this.scienceActionsList, 1, 2)

        padding = Insets(5.0)

    }

    private fun setFields() {
        this.moduleTypeLabel.text = ""
        this.locationLabel.text = ""
        this.scienceActionsList.items.clear()
        this.module?.also {
            this.locationLabel.text = "${it.location.row}, ${it.location.column}"
            this.moduleTypeLabel.text = "(not found)"
            it.techUnitType?.also {
                this.moduleTypeLabel.text = it.name
                this.scienceActionsList.items.setAll(it.actions)
            }

        }
    }
}