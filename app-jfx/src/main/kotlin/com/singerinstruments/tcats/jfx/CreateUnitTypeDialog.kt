package com.singerinstruments.tcats.jfx

import com.singerinstruments.tcats.data.UnitTypeData
import com.singerinstruments.tcats.model.ActionSource
import javafx.scene.control.ButtonType
import javafx.scene.control.Dialog
import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.layout.GridPane


class CreateUnitTypeDialog(scienceActionSource: ActionSource) : Dialog<UnitTypeData>() {

    init {
        dialogPane.buttonTypes.addAll(ButtonType.OK, ButtonType.CANCEL)
        val okButton = dialogPane.lookupButton(ButtonType.OK)
        okButton.isDisable = true
        isResizable = true

        val nameText = TextField().apply {
            textProperty().addListener { _, _, text -> okButton.isDisable = text.isNullOrEmpty() }
        }
        val actionsList = SelectActionsListView(scienceActionSource)

        val grid = GridPane()
        grid.add(Label("Name"), 0, 0)
        grid.add(nameText, 1, 0)
        grid.add(actionsList, 0, 1, 2, 1)

        dialogPane.content = grid

        setResultConverter { dialogButton ->
            if (dialogButton === ButtonType.OK) {
                UnitTypeData(nameText.text, actionsList.included)
            } else {
                null
            }
        }
    }
}

