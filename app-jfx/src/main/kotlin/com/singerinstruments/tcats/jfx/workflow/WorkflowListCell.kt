package com.singerinstruments.tcats.jfx.workflow

import com.singerinstruments.tcats.model.Workflow
import javafx.scene.control.Label
import javafx.scene.control.ListCell
import javafx.scene.control.Tooltip

/**
 * Represents an instance of Workflow in a ListView
 */
class WorkflowListCell : ListCell<Workflow?>() {
    private val label = Label()
    override fun updateItem(item: Workflow?, empty: Boolean) {
        super.updateItem(item, empty)
        this.label.text = ""
        this.label.tooltip = null
        if (empty) return
        item ?: return
        this.label.tooltip = Tooltip("uid: ${item.uid}")
        this.label.text = item.name
        this.graphic = label
    }
}

