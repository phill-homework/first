package com.singerinstruments.techwall.shared

import com.singerinstruments.kotlin.common.IconResource
import com.singerinstruments.kotlin.common.scaleFont
import com.singerinstruments.tcats.data.LocationData
import com.singerinstruments.tcats.data.UnitData
import com.singerinstruments.tcats.model.Action
import com.singerinstruments.tcats.model.TechUnit
import com.singerinstruments.tcats.model.TechUnitType
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.image.ImageView
import javafx.scene.layout.*
import javafx.scene.paint.Color
import javafx.scene.text.Text
import java.util.*


class UnitView(
    var row: Int = 0, var column: Int = 0,
    name: String,
    techUnitType: TechUnitType,
    var uid: String = UUID.randomUUID().toString()
) : VBox() {

    constructor(module: TechUnit)
            : this(module.location.row, module.location.column, module.name, module.techUnitType!!, module.uid)

    private val firstAction: Action? get() = this.techUnitType.actions.firstOrNull()

    var techUnitType: TechUnitType = techUnitType
        set(value) {
            field = value
            this.imageView.image = IconResource[firstAction?.uid ?: ""]
            this.typeLabel.text = firstAction?.name ?: ""
        }

    private val typeLabel = Label(firstAction?.name ?: "")
    private val nameTextField = Text(name).apply { scaleFont(1.25) }
    private val imageView = ImageView(IconResource[firstAction?.uid ?: ""]).apply {
        fitWidth = 30.0
    }

    val moduleData: UnitData
        get() {
            if (this.uid.isEmpty()) {
                // If we created this ourselves then the uid will be blank, so we can let the unitData
                // constructor create a suitable one according to its own rules.
                return UnitData(this.name, this.techUnitType.uid, LocationData(this.row, this.column))
            }
            return UnitData(this.name, this.techUnitType.uid, LocationData(this.row, this.column), this.uid)
        }

    var name
        get() = this.nameTextField.text
        set(value) {
            this.nameTextField.text = value
        }

    init {
        this.alignment = Pos.TOP_CENTER
        this.alignment = Pos.TOP_CENTER
        this.minHeight = /*this.element.moduleHeight **/ 100.0

        val title = HBox(nameTextField, imageView).apply {
            alignment = Pos.CENTER
            spacing = 4.0
        }

        val uidLabel = Label(this.uid).apply {
            scaleFont(0.5)
        }

        this.children.addAll(title, this.typeLabel, uidLabel)

        imageView.isPreserveRatio = true
        this.border = Border(BorderStroke(Color.RED, BorderStrokeStyle.SOLID, CornerRadii(5.0), BorderWidths(1.0)))
        this.background = Background(BackgroundFill(Color.color(1.0, 0.95, 0.975), CornerRadii(5.0), Insets.EMPTY))

        this.padding = Insets(0.0, 8.0, 0.0, 8.0)
        this.padding = Insets(4.0)

        this.onContextMenuRequested = EventHandler {

        }
    }
}

