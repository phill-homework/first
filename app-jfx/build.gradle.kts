
plugins {
    id("com.singerinstruments.tcats.kotlin-application-conventions")
    id("org.openjfx.javafxplugin") version "0.0.8" // PTvL
    id("com.github.johnrengelman.shadow") version "6.1.0"
}

javafx {
    version = "11.0.2"
    modules = mutableListOf("javafx.controls") //PTvL
}

application {
    // https://github.com/johnrengelman/shadow/issues/336
    @Suppress("DEPRECATION")
    mainClassName ="com.singerinstruments.tcats.jfx.Main"
    mainClass.set("com.singerinstruments.tcats.jfx.Main")
}

tasks.withType<Jar> {
    manifest {
        attributes["Main-Class"] = "com.singerinstruments.tcats.jfx.Main"
    }
}

// Minimum jvmTarget of 1.8 needed since Kotlin 1.1
tasks.compileKotlin {
    kotlinOptions.jvmTarget = "11"
}
tasks.compileTestKotlin {
    kotlinOptions.jvmTarget = "11"
}

dependencies {
    implementation(project(":viewmodel"))
    implementation(project(":sijfx"))
    implementation(project(":model"))
    implementation(project(":data"))
    implementation(project(":dataservices"))
}
